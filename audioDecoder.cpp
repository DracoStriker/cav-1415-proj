#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sndfile.h>
#include <fstream>
#include <sstream>
#include <ctime>
#include <getopt.h>
#include <sys/stat.h>
#include <iomanip>

#include "libCoding/GolombRiceDecoderAudio.hpp"
#include "libCoding/IntraChannelAudioPredictor.hpp"
#include "libCoding/InterChannelAudioPredictor.hpp"
#include "libCoding/AdvancedChannelAudioPredictor.hpp"

void printUsage(void);
int parseOpts(int, char**);
bool fexists(const string&);

string outFileName = "";
string inFileName = "";

int main(int argc, char **argv)
{

	SNDFILE *soundFileOut; /* Pointer for output sound file */
	SF_INFO soundInfoOut; /* Output sound file Info */

	clock_t start;
	double decDuration;

	int i = 0;
	short sample[2];
	sf_count_t nSamples = 1;
	unsigned short m;

	/* Check number of arguments */
	/*if (argc < 3){
		fprintf(stderr, "Usage: audioDecoder <input file> <output file>\n");
		return -1;
	}*/

	/* Parse parameters */
	if (parseOpts(argc, argv)) return -1;

	/* Check if file exists */
	if (!fexists(inFileName)) {
            cerr << "Error opening file " << inFileName << " or file does not exist..." << endl;
            return -1;
    }

	ifstream fileIn(inFileName);
	int frames, blockSize, advanced;
	int inter;

	/* Reading audio file parameters as well as the type of encoding done */
	string line;
    getline(fileIn, line);
	istringstream(line) >> m >> frames >> soundInfoOut.samplerate >> soundInfoOut.channels >> soundInfoOut.format >> inter >> advanced >> blockSize;

	/* Printing important audio file info */
	fprintf(stderr, "Frames (samples): %d\n", frames);
	fprintf(stderr, "Samplerate: %d\n", soundInfoOut.samplerate);
	fprintf(stderr, "Channels: %d\n", soundInfoOut.channels);
	fprintf(stderr, "Format: %d\n", soundInfoOut.format);
	fprintf(stderr, "Advanced: %d\n", advanced);
	fprintf(stderr, "BlockSize: %d\n", blockSize);

	/*-----------------------------Advanced--------------------------------*/
	/* Initialize structures needed for the advanced algorithm */
	AdvancedChannelAudioPredictor advCAP(blockSize,soundInfoOut.channels);

	int numBlocks = frames/blockSize +1;
	int lastBlockSize = frames%blockSize;
	int pastSamples [2][N_INTRA_PREDICTORS-1];
	int* linExpCh0;
	int* linExpCh1;
	int blkIdx=0;

	/*-----------------------------Advanced--------------------------------*/
	
	/* Golomb instantiation*/
	GolombRiceDecoderAudio golomb(m, &fileIn);

	/* Advanced coding */
	if(advanced){

		/* Memory allocation for types of predictors used on file */
		linExpCh0 = new int[numBlocks];
		linExpCh1 = new int[numBlocks];

		/* Decode the linear expressions used for each block */
		for(i = 0; i < numBlocks; i++){
			linExpCh0[i] = IntraChannelAudioPredictor::unconvert(golomb.decoder());
			linExpCh1[i] = IntraChannelAudioPredictor::unconvert(golomb.decoder());
		}
		i = 0;
	}

	/* When opening a file for write, it is necessary to fill in the
	 * structure members samplerate, channels and format.
	 */
	soundFileOut = sf_open (outFileName.c_str(), SFM_WRITE, &soundInfoOut);

	if (soundFileOut == NULL){
		fprintf(stderr, "Could not open file for writing: \"%s\"\n", argv[2]);
		return -1;
	}

	/* Intra and Inter Channel instantiation*/
	InterChannelAudioPredictor intercap;
	IntraChannelAudioPredictor icap;
	
	cout << "Start Decoding..." << endl;

	/* Start clock saving decoding time */
	start = clock();

	/* Going through the file, reading and decoding values */
	while (!fileIn.eof()) {

		/* Advanced alg decoder */
		if(advanced){

			/* Reset block index for new block and reset pastSamples values cause blocks are independent */
			if( blkIdx == blockSize ){
				for(int l = 0; l < (N_INTRA_PREDICTORS-1); l++){
					pastSamples[0][l] = 0;
					pastSamples[1][l] = 0;
				}
				blkIdx=0;
			}

			/* Decode the values of each channel of a frame */
			int val0 = IntraChannelAudioPredictor::unconvert(golomb.decoder());
			int val1 = IntraChannelAudioPredictor::unconvert(golomb.decoder());
			sample[0] = advCAP.decode( val0, val1, linExpCh0[i/blockSize], pastSamples[0], blkIdx, 0);
			sample[1] = advCAP.decode( val1, val0, linExpCh1[i/blockSize], pastSamples[1], blkIdx, 1);

			//if (linExpCh0[i/blockSize] == 5) {
			//	cout << val0 << ", " << val1 << " - " << sample[0] << ", " << sample[1] << endl;
			//}


			/* Update values of past Samples */
			for(int k=0; k<(N_INTRA_PREDICTORS-2); k++){
				pastSamples[0][k] = pastSamples[0][k+1];
				pastSamples[1][k] = pastSamples[1][k+1];
			}
			pastSamples[0][N_INTRA_PREDICTORS-2]=sample[0];
			pastSamples[1][N_INTRA_PREDICTORS-2]=sample[1];

		/* Inter frame decoding */	
		}else if (inter) {
			sample[0] = icap.decode(golomb.decoder(), 0);
			sample[1] = intercap.decode(golomb.decoder(), sample[0]);
		/* Intra frame decoding */	
		} else {
			sample[0] = icap.decode(golomb.decoder(), 0);
			sample[1] = icap.decode(golomb.decoder(), 1);
		}	
		
		/* Writing to an audio file */
		if (sf_writef_short(soundFileOut, sample, nSamples) != 1) {
			fprintf(stderr, "Error writing frames to the output:\n");
			fileIn.close();
			sf_close(soundFileOut);
			return -1;
		}
		i++;		//increment frame counter
		blkIdx++;	//increment block index
	}

	/* Compression duration*/
	decDuration = ( clock() - start ) / (double) CLOCKS_PER_SEC;

	cout << "Finished Decoding." << endl;
	cout << setprecision(3) << "Decoding duration: " << decDuration << " sec" << endl;

	/* Closing files*/
	fileIn.close();
	sf_close(soundFileOut);

	return 0;
}


/**
 * Parse input parameters
 * @return <li> 0, success parsing arguments
		   <li> 1, failure parsing arguments
 */
int parseOpts(int argc, char** argv) {
    int c;
    while ((c = getopt(argc, argv, "f:o:h")) != -1) {
        switch (c) {
        	break;
            case 'f':
                inFileName = optarg;
                break;
            case 'o':
                outFileName = optarg;
                break;
            break;    
            case 'h':
            default:
                printUsage();
                return -1;
        }
    }
    return 0;
}

/**
 * Print usage
 *
 */
void printUsage(void) {
    cout << "audioDecoder -f <input filename> <output filename>" << endl;
}

/**
 * Checks if a given file exists
 * @return <li> 0, failure, file does not exist
		   <li> 1, success, file exists
 */
bool fexists(const string& filename) {
    ifstream ifile(filename.c_str());
    return ifile;
}
