/* 
 * File:   videoComp.cpp
 * Author: simon
 *
 * Created on October 2, 2014, 2:05 AM
 */

#include <getopt.h>
#include <cmath>
#include <unistd.h>

#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/Video.hpp"
#include "libRGBYUV/VideoFromCamera.hpp"
#include "libRGBYUV/VideoFromFile.hpp"
#include "libPlot/cvplot.h"

using namespace std;

double max(vector<double> &);
double avg(vector<double> &);
double psnr(Frame &, Frame &, unsigned int, unsigned int);
void printUsage();
void parseOpt(int, char**);
void plotSamples(vector<double> &);
bool fexists(const string&);

string fileName1, fileName2;
int dev1, dev2, n;
bool isDev1, isDev2, plot = false;

/**
 * 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char** argv) {

    vector<double> samples;
    samples.push_back(0);
    Video *v1;
    Video *v2;
    Frame *f1;
    Frame *f2;
    int N;
    int M;

    parseOpt(argc, argv);

    if (n != 2) {
        printUsage();
        return 0;
    }

    if (isDev1) {
        v1 = new VideoFromCamera(dev1);
    } else {
        if (!fexists(fileName1)) {
            cerr << "Error opening file  " << fileName1 << " or file does not exist" << endl;
            return -1;
        }
        v1 = new VideoFromFile(fileName1);
    }

    if (isDev2) {
        v2 = new VideoFromCamera(dev2);
    } else {
        if (!fexists(fileName2)) {
            cerr << "Error opening file  " << fileName2 << " or file does not exist" << endl;
            return -1;
        }
        v2 = new VideoFromFile(fileName2);
    }

    if ((v1->getNCols() != v2->getNCols()) || (v1->getNRows() != v2->getNRows())) {
        cout << "The two videos must have the same dimension..." << endl;
        return 0;
    }

    f1 = v1->genFrame();
    f2 = v2->genFrame();

    N = v1->getNRows();
    M = v1->getNCols();

    int frame = 0;
    bool isDev = isDev1 || isDev2;
    while (!v1->readFrame(*f1) && !v2->readFrame(*f2)) {

        cout << frame++ << endl;

        /* compare frames */
        samples.push_back(psnr(*f1, *f2, N, M));

        /* if camera press key to stop */
        if (isDev) usleep(1.0 / v1->getFps() * 1000000);
    }

    cout << "PSNR = " << avg(samples) << endl;

    if (plot) {
        plotSamples(samples);
    }

    return 0;
}

/**
 * 
 * @param samples
 * @return 
 */
double max(vector<double> &samples) {
    double max = 0.0;
    unsigned long size = samples.size();
    for (long i = 0; i < size; i++) {
        if (max < samples[i]) {
            max = samples[i];
        }
    }
    return max;
}

/**
 * 
 * @param samples
 * @return 
 */
double avg(vector<double> &samples) {
    double total;
    unsigned long size = samples.size();
    for (long i = 0; i < size; i++) {
        total += samples[i];
    }
    return total / (double) size;
}

/**
 * 
 * @param f1
 * @param f2
 * @param N
 * @param M
 * @return 
 */
double psnr(Frame &f1, Frame &f2, unsigned int N, unsigned int M) {
    double A2 = 255.0 * 255.0;
    double e2 = 0.0;
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < M; c++) {
            int ch0 = f1.getPixelValChannel(r, c, 1) - f2.getPixelValChannel(r, c, 1);
            int ch1 = f1.getPixelValChannel(r, c, 2) - f2.getPixelValChannel(r, c, 2);
            int ch2 = f1.getPixelValChannel(r, c, 3) - f2.getPixelValChannel(r, c, 3);
            e2 += ch0 * ch0 + ch1 * ch1 + ch2 * ch2;
        }
    }
    e2 /= N * M;
    return 10 * log10(A2 / e2);
}

/**
 * 
 */
void printUsage() {
    cout << "videoComp" << endl << "-c : camera device" << endl << "-f : video file" << endl << "-p : plot" << endl;
}

/**
 * 
 * @param argc
 * @param argv
 */
void parseOpt(int argc, char** argv) {
    int c;
    n = 0;
    while ((c = getopt(argc, argv, "c:f:hp")) != -1) {
        switch (c) {
            case 'c':
                if (n == 0) {
                    isDev1 = true;
                    dev1 = atoi(optarg);
                } else if (n == 1) {
                    isDev2 = true;
                    dev2 = atoi(optarg);
                }
                n++;
                break;
            case 'f':
                if (n == 0) {
                    isDev1 = false;
                    fileName1 = optarg;
                } else if (n == 1) {
                    isDev2 = false;
                    fileName2 = optarg;
                }
                n++;
                break;
            case 'p':
                plot = true;
                break;
            case 'h':
            default:
                printUsage();
        }
    }
}

void plotSamples(vector<double> &samples) {
    CvPlot::plot("PSNR results - "+fileName1+" compared to "+fileName2, (double*) samples.data(), (int) samples.size(), 1, 0, 255, 0);
	CvPlot::label("PSNR");
    waitKey(0);
}

/**
 * 
 * @param filename
 * @return 
 */
bool fexists(const string& filename) {
    ifstream ifile(filename.c_str());
    return ifile;
}
