#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sndfile.h>
#include <getopt.h>
#include <fstream>
#include <ctime>
#include <sys/stat.h>
#include <iomanip>
#include <vector>

#include "libCoding/GolombRiceEncoderAudio.hpp"
#include "libCoding/IntraChannelAudioPredictor.hpp"
#include "libCoding/InterChannelAudioPredictor.hpp"
#include "limits.h"
#include "libPlot/cvplot.h"
#include "libCoding/AdvancedChannelAudioPredictor.hpp"

#define hValue(x) x+2*(-SHRT_MIN)
#define HIST_VALUES_SIZE (int) 2*(SHRT_MAX+(-SHRT_MIN))+1

using namespace std;

void printUsage(void);
int parseOpts(int, char**);
size_t getFilesize(const char*);
bool fexists(const string&);

int inter = 0;
bool plot = false;
int advanced = 0;
int blockSize = 500;		//default value
unsigned short m = 512;
string outFileName = "";
string inFileName = "";
vector<int> valuesC0(HIST_VALUES_SIZE);
vector<int> valuesC1(HIST_VALUES_SIZE);
vector<int> resValuesC0(HIST_VALUES_SIZE);
vector<int> resValuesC1(HIST_VALUES_SIZE);

int main(int argc, char **argv)
{

	SNDFILE *soundFileIn; 	/* Pointer for input sound file */
	SF_INFO soundInfoIn; 	/* Input sound file Info */

	clock_t start;
	double encDuration;
	int i;
	double ratio, fileOutSize, fileInSize;
	short sample[2];
	sf_count_t nSamples = 1;

	/* Parse parameters */
	if (parseOpts(argc, argv)) return -1;

	/* Check if file exists */
	if (!fexists(inFileName)) {
            cerr << "Error opening file " << inFileName << " or file does not exist..." << endl;
            return -1;
    }

	/* When opening a file for read, the format field should be set to zero
	 * before calling sf_open(). All other fields of the structure are filled
	 * in by the library
	 */	
	soundInfoIn.format = 0;
	soundFileIn = sf_open (inFileName.c_str(), SFM_READ, &soundInfoIn);
	
	if (soundFileIn == NULL){
		fprintf(stderr, "Could not open file for reading: \"%s\"\n",
		  inFileName.c_str());
		sf_close(soundFileIn);
		return -1;
	}

	/* Initialize variables and structures for advanced encoding */
	int nChannels = 2, fixed;
	int lastBlockSize;						//size of last block
	int last = 0;							//flag signalling if last block
	int numBlocks;		
	int nFr = (int) soundInfoIn.frames;		// number of frames
	int contFrames = 0;						//frame counter

	numBlocks = nFr/blockSize +1;
	fixed = blockSize;
	lastBlockSize = nFr%blockSize;
	
	int* linExpCh0 = new int[numBlocks];
	int* linExpCh1 = new int[numBlocks];
	short** blockSourceVals =  new short*[nChannels];
	int** blockResidualVals =  new int*[nChannels];

	for(i = 0; i < nChannels; i++){
		blockSourceVals[i] = new short[blockSize];
		blockResidualVals[i] = new int[(numBlocks-1)*blockSize + lastBlockSize];
	}
	
	/* Instantiation of advanced coding */
	AdvancedChannelAudioPredictor advCAP(blockSize,soundInfoIn.channels);

	/* Printing important audio file info */
	fprintf(stderr, "Frames (samples): %d\n", (int) soundInfoIn.frames);
	fprintf(stderr, "Samplerate: %d\n", soundInfoIn.samplerate);
	fprintf(stderr, "Channels: %d\n", soundInfoIn.channels);
	fprintf(stderr, "Format: %d\n", soundInfoIn.format);

	string name = outFileName + ".agmb";
	ofstream fileOut(name); 			/* Output file */

	fileOut << m << " " << soundInfoIn.frames << " " << soundInfoIn.samplerate << " " << soundInfoIn.channels << " " << soundInfoIn.format << " " << inter << " " << advanced << " " << blockSize << endl;

	/* Golomb instantiation */
	GolombRiceEncoderAudio golomb(m, &fileOut);

	/* Intra and Inter Channel instantiation*/
	InterChannelAudioPredictor intercap;
	IntraChannelAudioPredictor icap;
	

	if (!fileOut) {
		fprintf(stderr, "Could not open file for writing: \"%s\"\n", outFileName.c_str());
		sf_close(soundFileIn);
		return -1;
	}
	cout << "Start Encoding..." << endl;
	
	/* Start clock saving encoding time */
	start = clock();
	
	if(advanced){
		int j;
		for(i = 0; i < numBlocks; i++){

			/* Change size of last block, cause last block may not be totally filled */
			if(i == numBlocks-1){
				last = 1;
				blockSize = nFr - contFrames;
			}

			/* Gather vals from source to block, fill the block array with original values to be predicted */
			for (j = 0; j < blockSize; j++)
			{
				contFrames++;		//frame counter
				if (sf_readf_short(soundFileIn, sample, nSamples) == 0) {
					fprintf(stderr, "Error: Reached end of file\n");
					sf_close(soundFileIn);
					fileOut.close();
					break;
				}

				/* Save read vals from source to an array*/
				blockSourceVals[0][j] = sample[0];
				blockSourceVals[1][j] = sample[1];
				
				/* Fill histogram with original sample values */
				valuesC0[hValue(sample[0])]++;
				valuesC1[hValue(sample[1])]++;

			}

			/* Fill array with original values of a given block for each channel */
			advCAP.fillBlock(blockSourceVals[0], 0);
			advCAP.fillBlock(blockSourceVals[1], 1);

			/* Channel 0*/
			/* Calculates the best predictor for the given block */
			linExpCh0[i] = advCAP.encode(0);		

			/* Copies the values of a block to an array to be coded with golomb */
			memcpy( &blockResidualVals[0][i*fixed], advCAP.getBestPredVals(), blockSize*sizeof(int) );
			
			/* Channel 1*/
			/* Calculates the best predictor for the given block */
			linExpCh1[i] = advCAP.encode(1);

			/* Copies the values of a block to an array to be coded with golomb */
			memcpy( &blockResidualVals[1][i*fixed], advCAP.getBestPredVals(), blockSize*sizeof(int) );

			//if (linExpCh0[i] == 5) {
			//	for (int b = 0; b < blockSize; b++)
			//		cout << blockResidualVals[0][i*fixed+b] << ", " << blockResidualVals[1][i*fixed+b] << " - " << blockSourceVals[0][b] << ", " << blockSourceVals[1][b] << endl;
			//}
		}
		
		/* Write linear expression chosen per frame [ch0] [ch1] */
		for(i = 0; i < numBlocks; i++){
			golomb.encoder( IntraChannelAudioPredictor::convert( linExpCh0[i] ) );	// channel 0
			golomb.encoder( IntraChannelAudioPredictor::convert( linExpCh1[i] ) );	// channel 1
		}

		/* Golomb coding of residuals */
		for(i = 0; i < soundInfoIn.frames; i++){
			/* Coding residuals with golomb and writing to file */
			golomb.encoder( IntraChannelAudioPredictor::convert( blockResidualVals[0][i] ) );
			golomb.encoder( IntraChannelAudioPredictor::convert( blockResidualVals[1][i] ) );
		}
		
		/* Filling histogram */
		for(i=0; i < numBlocks; i++){
			resValuesC0[hValue(blockResidualVals[0][i])]++;
			resValuesC1[hValue(blockResidualVals[1][i])]++;
		}

	/* Intra and Inter Channel standard coding*/
	}else{
		for (i = 0; i < soundInfoIn.frames ; i++)
		{
			//cout << "\r" << i;

			if (sf_readf_short(soundFileIn, sample, nSamples) == 0) {
				fprintf(stderr, "Error: Reached end of file\n");
				sf_close(soundFileIn);
				fileOut.close();
				break;
			}
			/* Inter channel coding */
			if (inter) {
				/* Coding sample 0 with Golomb, intra channel coding (icap) */
				golomb.encoder(icap.encode(sample[0], 0));	
				
				/* Histogram calculation Ch0 */
				valuesC0[hValue(sample[0])]++;				
				resValuesC0[hValue(icap.getDelta())]++;
				
				/* Coding sample 1 with Golomb, inter channel coding (intercap) */
				golomb.encoder(intercap.encode(sample[1], sample[0]));
				
				/* Histogram calculation Ch1 */
				valuesC1[hValue(sample[1])]++;		
				resValuesC1[hValue(intercap.getDelta())]++;

			/* Intra channel coding */	
			} else {
				/* Coding sample 0 with Golomb, intra channel coding (icap) */
				golomb.encoder(icap.encode(sample[0], 0));
				
				/* Histogram calculation Ch0 */
				valuesC0[hValue(sample[0])]++;
				resValuesC0[hValue(icap.getDelta())]++;
				
				/* Coding sample 1 with Golomb, intra channel coding (icap) */
				golomb.encoder(icap.encode(sample[1], 1));

				/* Histogram calculation Ch1 */
				valuesC1[hValue(sample[1])]++;
				resValuesC1[hValue(icap.getDelta())]++;
			}	
		}
	}

	golomb.finalize();

	/* Compression duration*/
	encDuration = ( clock() - start ) / (double) CLOCKS_PER_SEC;

	cout << "Finished Encoding." << endl;

	/* Closing files*/
	sf_close(soundFileIn);
	fileOut.close();

	/* Preparing and printing results */
	fileOutSize = getFilesize(name.c_str());
	fileInSize = getFilesize(inFileName.c_str());
	ratio = fileOutSize/fileInSize;

	cout << setprecision(3);
	cout << "FileOutSize:\t" << "FileInSize:" << endl << (double) getFilesize(name.c_str())/(1024.0*1024.0) << " (MB)\t" << 
		 		(double) getFilesize(inFileName.c_str())/(1024.0*1024.0) << " (MB)" << endl;
	cout << "Encoding duration: " << encDuration << " sec" << endl << "Compression ratio: " <<  ((1-ratio)*100) << " %" << endl;
	
	/* Histogram show */
	if(plot){
		CvPlot::plot("Histogram - Values Channel 0", (int*) valuesC0.data(), (int) valuesC0.size(), 1, 255, 0, 0);
		CvPlot::label("Channel 0");
		CvPlot::plot("Histogram - Values Channel 1", (int*) valuesC1.data(), (int) valuesC1.size(), 1, 255, 0, 0);
		CvPlot::label("Channel 1");
		CvPlot::plot("Histogram - Residual Values Channel 0", (int*) resValuesC0.data(), (int) resValuesC0.size(), 1, 0, 0, 255);
		CvPlot::label("Channel 0");
		CvPlot::plot("Histogram - Residual Values Channel 1", (int*) resValuesC1.data(), (int) resValuesC1.size(), 1, 0, 0, 255);
		CvPlot::label("Channel 1");
		cvWaitKey(0);
	}

	return 0;
}

/**
 * Get the size of a file.
 * @return The filesize, or 0 if the file does not exist.
 */
size_t getFilesize(const char* filename) {
    struct stat st;
    if(stat(filename, &st) != 0) {
        return 0;
    }
    return st.st_size;   
}

/**
 * Print usage
 *
 */
void printUsage(void) {
    cout << "audioEncoder -f <filename> -o <filename> [-m <golomb>] [-b <blockSize>] [-i \"inter\"] [-a\"advanced\"] [-p \"plot\"]" << endl;
}


/**
 * Parse input parameters
 * @return <li> 0, success parsing arguments
		   <li> 1, failure parsing arguments
 */
int parseOpts(int argc, char** argv) {
    int c;
    while ((c = getopt(argc, argv, "af:o:m:b:ihp")) != -1) {
        switch (c) {
        	break;
            case 'a':
            	advanced = 1;
            	break;    
            case 'b':
            	blockSize = atoi(optarg);
            	break;
            case 'f':
                inFileName = optarg;
                break;
            case 'i':
                inter = 1;
                break;
            case 'o':
                outFileName = optarg;
                break;
            case 'm':
                m = atoi(optarg);
                break;
            case 'p':
				plot=true;
            break;    
            case 'h':
            default:
                printUsage();
                return -1;
        }
    }
    return 0;
}

/**
 * Checks if a given file exists
 * @return <li> 0, failure, file does not exist
		   <li> 1, success, file exists
 */
bool fexists(const string& filename) {
    ifstream ifile(filename.c_str());
    return ifile;
}
