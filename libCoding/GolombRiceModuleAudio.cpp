/* 
 * File:   golombModule.cpp
 * Author: nuno
 * 
 * Created on November 11, 2014, 6:23 PM
 */

#include "GolombRiceModuleAudio.hpp"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/*
GolombRiceModule::GolombRiceModule(unsigned int mParam){
    /* Initialization of variables and buffer
    m = mParam;
    bw_index = 0;
    maxBufferSize = ((255/m + log2(m) + 1) / 8) + 1;

    buf = new unsigned char [maxBufferSize];
    
    /* Creation of bitwrapper
    bitWrapper = new bitwrapper(buf, maxBufferSize);
}

*/
GolombRiceModuleAudio::~GolombRiceModuleAudio() {

}



bool GolombRiceModuleAudio::isPowerOf2(unsigned int x) {
    return ( (x != 0) && ((x & (x - 1)) == 0));
}


