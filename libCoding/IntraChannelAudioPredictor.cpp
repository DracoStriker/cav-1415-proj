
#include "IntraChannelAudioPredictor.hpp"
#include "../libBit/bitwrapper.hpp"

IntraChannelAudioPredictor::IntraChannelAudioPredictor() {
	previous[0] = 0;
	previous[1] = 0;
}

IntraChannelAudioPredictor::~IntraChannelAudioPredictor() {
}

unsigned int IntraChannelAudioPredictor::encode(short sample, unsigned int ch) {
	delta = sample - previous[ch]; 
	previous[ch] = sample;
	return convert(delta);
}

short IntraChannelAudioPredictor::decode(unsigned int codeword, unsigned int ch) {
	delta = unconvert(codeword);
	short sample = delta + previous[ch];
	previous[ch] = sample;
	return sample;
}

unsigned char IntraChannelAudioPredictor::getSignal(int a){
	return a<0 ? 1 : 0;
}

unsigned int IntraChannelAudioPredictor::getModule(int a){
	return a>=0 ? a : -a;
}

int IntraChannelAudioPredictor::min(int a, int b){
	return a<b ? a:b;
}

int IntraChannelAudioPredictor::max(int a, int b){
	return a>b ? a:b;
}

unsigned int IntraChannelAudioPredictor::convert(int in){
	unsigned char signal;
	unsigned int out;
	
	signal=getSignal(in);
	out= 0xFFFF & getModule(in);
        
        //cout <<"signal: "<<(unsigned int) signal<<" | value: "<<out<<endl;
        
	bitwrapper bw((unsigned char *) &out, sizeof(int));
	bw.write(16,signal);
	return out;
}

int IntraChannelAudioPredictor::unconvert(unsigned int in){
	unsigned char signal;
	int out=0xFFFF & in;
	
	bitwrapper bw((unsigned char *) &in, sizeof(unsigned int));
	signal=bw.read(16);
        
	if(signal){
		//cout <<"signal: "<<(unsigned int) signal<<" | value: "<<out<<endl;
	    return -out;
	}
	//cout <<"signal: "<<(unsigned int) signal<<" | value: "<<out<<endl;
	return out;
        
	//return out;
}

int IntraChannelAudioPredictor::getDelta(){
	return delta;
}
