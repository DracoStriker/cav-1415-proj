/* 
 * File:   golombEncoder.cpp
 * Author: nuno
 * 
 * Created on November 15, 2014, 4:15 PM
 */

#include "GolombRiceEncoder.hpp"

using namespace std;

GolombRiceEncoder::GolombRiceEncoder(unsigned int mParam, ostream* os){
    /* Initialization of variables and buffer */
    m = mParam;
    bw_index = 0;
    maxBufferSize = ((255/m + log2(m) + 1) / 8) + 1;

    buf = new unsigned char [maxBufferSize];
    
    /* Creation of bitwrapper */
    bitWrapper = new bitwrapper(buf, maxBufferSize);

    /* Creation of obitstream */
    obs = new obitstream(os);
    
}

GolombRiceEncoder::~GolombRiceEncoder() {
    //delete obs;
    //delete bitWrapper;
    //delete[] buf;
}

int GolombRiceEncoder::encoder(unsigned short pred){
    
    /* Resetting Variables */    
    aux = bw_index = remainder = quotient = 0;
    positive = false;
    
    /*  Extract signal of value received */
    aux = pred & 0x00000100;
    if (aux == 0)
        positive = true;
    
    /* Getting module of value received */
    pred &= 0x00FF;
        
    /*  Calculate the quotient */
    quotient = pred / m;

    /*  Calculate the remainder */
    remainder = pred % m;
    
    //cout << " N = " << pred << " M = " << m << " Quotient = " << quotient << " Remainder = " << remainder << endl;

    /*  Generate Codeword */

    /* Quotient code */
    decodeAndWriteValUnary(quotient);   //Writing quotient into bitWrapper

    /* Remainder code - in truncated binary encoding*/
    b = log2(m);    //calculation of variable m

    /* Binary format */
    if ( isPowerOf2(m) ) {
        nbits = b;          // length of word (Rice code)
        binary = remainder;
    } else {
        b += 1;
        l = (pow(2, b) - m);
        if (remainder < l) {
            nbits=b-1;
            binary = remainder;

        }else{ 
            binary = remainder+l;
            nbits = b;
        }
    }
    //cout << "CodewordSize " << codewordSize << "nBits= " << nbits << " binary " << binary <<endl;
    
    /* Calculate number of bits to write */
    codewordSize = 1+ (quotient+1) + nbits; // 1 signal bit; nbits is numBits of remainder 
       
    /* Write to bitwrapper value of the remainder(binary variable) */
    for(int i = nbits; i > 0; i--){
        aux = exp2(i-1);
        bit = ( (binary & aux) != 0 ) ? 1 : 0;
        bitWrapper->write(bw_index++, bit);
    }
    
        /* Insert sign bit into codeword - 0->Positive 1->Negative*/
    if(pred != 0){
      bit = (positive) ? 0 : 1;
      bitWrapper->write(bw_index++, bit);
    }else{
      codewordSize--;
    }
    
    /* Write calculated code to file */
    obs->write(*bitWrapper, codewordSize);
    
    /* TEST BITWRAPPER VALUES AND PRINT IT */
    //cout << "Codeword on bitWrapper:";
    //for(int i = 0; i < bw_index; i++)
        //cout << " " << (unsigned int) bitWrapper->read(i);
    //cout << endl;

    return codewordSize;

}

void GolombRiceEncoder::decodeAndWriteValUnary(unsigned int q){
    //unsigned char bit = 1;
    /* Writing q times bit 1*/
    for(int i = 0; i < q; i++)
        bitWrapper->write(bw_index++, (unsigned char) 1);
    
    /* Writing 0 after number of ones */
    bitWrapper->write(bw_index++, (unsigned char) 0);
    
}

void GolombRiceEncoder::finalize(){
   
    obs->finalize();
    
}
