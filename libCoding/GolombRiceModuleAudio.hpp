/* 
 * File:   golombModule.hpp
 * Author: nuno
 *
 * Created on November 11, 2014, 6:23 PM
 */
#include "../libBit/bitwrapper.hpp"
#include "../libBit/obitstream.hpp"
#include "../libBit/ibitstream.hpp"
#include <cmath>
#include <stdio.h>
#include <iostream>
#include <climits>

#ifndef GOLOMBRICEMODULEAUDIO_HPP
#define	GOLOMBRICEMODULEAUDIO_HPP

class GolombRiceModuleAudio {
public:
    //GolombRiceModule(unsigned int m);
   
    //golombModule(const golombModule& orig);
    virtual ~GolombRiceModuleAudio();
    
   
protected:
    /**
     * Checks if a given value is a power of 2
     * @param x Value to verify if it is power of 2
     * @return if true, x is power of 2
     *         if false, x is not power of 2 
     */
    bool isPowerOf2(unsigned int x);
    
    /** Properties */
    
    /** Constants used in encoding and decoding */
    unsigned int l, b, m;
    /** Auxiliary variables of the encoder */
    unsigned int quotient, remainder, nbits, aux;
    unsigned int binary, codeword, codewordSize;
    
    /** Value of bit to be written */
    unsigned char bit;
    /** Indicates signal of value received  */
    bool positive;
    /** max buffer size */
    unsigned int maxBufferSize;
    /** Pointer to buffer to be included in bitWrapper with codeword */
    unsigned char * buf;
    /** Pointer to bitWrapper */
    bitwrapper *bitWrapper;
    /** BiwWrapper current index */
    unsigned int bw_index;
    /** Pointer to a obitstream in order to write bits to a file */
    obitstream *obs;
    
};

#endif	/* GOLOMBRICEMODULEAUDIO_HPP */

