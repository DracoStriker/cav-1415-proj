/* 
 * File:   JpegLs.cpp
 * Author: pedro
 * 
 * Created on November 11, 2014, 5:17 PM
 */

#include "JpegLs.hpp"
#include "../libRGBYUV/Frame.hpp"
#include "../libBit/bitwrapper.hpp"
#include "../libRGBYUV/YUV420_Frame.hpp"
#include <limits.h>

JpegLs::JpegLs() {
  lossy = 0;
  pos = posNewFrame = 0;	// initialize indexes
}

JpegLs::JpegLs(int lossyFlag, int factY, int factU, int factV){
  /* Initialize variables*/
  lossy = lossyFlag;		// lossy - 1 or lossless - 0
  factorY = factY;	factorU = factU; factorV = factV;	//lossy factors
  pos = posNewFrame = 0;	// initialize indexes
}

JpegLs::~JpegLs() {
}

void JpegLs::encode(Frame *f, unsigned short * buffer){
	
	unsigned char *y, *y1;
	unsigned char *u, *u1;
	unsigned char *v, *v1;
	
	pos = posNewFrame = 0;	// initialize indexes

	//int lossy = 0, posNewFrame = 0;	
	//int factor1, factor2, factor3;
	//factor1 = 10;	factor2 = 10;	factor3 = 10;
	
	/* Create new frame */
	Frame *aux = new YUV420_Frame(f->getNRows(), f->getNCols());
	
	/* Get pointers to each component of original and auxiliary frame */
	f->getChannels(&y,&u,&v);
	aux->getChannels(&y1,&u1,&v1);
	
	//cout << "encode" << endl;
    if(lossy){
      /* Calculating values of Y component of original frame */
      for (int i = 0; i < f->getXRows(); i++) {
            for (int j = 0; j < f->getXCols(); j++) { 
            	//cout << "(" << i << ", " << j << ")" << endl;
				x=y[i*f->getXCols()+j];				 
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=y1[i*f->getXCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=y1[(i-1)*f->getXCols()+j];
					c=0;
				}
				else{
					a=y1[i*f->getXCols()+j-1];
					b=y1[(i-1)*f->getXCols()+j];
					c=y1[(i-1)*f->getXCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                                //cout <<"convert "<<x<<"-"<<aX<<"   ";
				y1[posNewFrame] =  (((x-aX)/factorY)*factorY) +aX;
				buffer[pos]=convert((x-aX)/factorY);
				/* Increment indexes of new frame and buffer*/
				posNewFrame++;	pos++;
			}
		}
		posNewFrame = 0;	//reset component index of new frame
		/* Calculating values of U component of original frame */
		for (int i = 0; i < f->getYRows(); i++) {
            for (int j = 0; j < f->getYCols(); j++) { 
				x=u[i*f->getYCols()+j];				 
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=u1[i*f->getYCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=u1[(i-1)*f->getYCols()+j];
					c=0;
				}
				else{
					a=u1[i*f->getYCols()+j-1];
					b=u1[(i-1)*f->getYCols()+j];
					c=u1[(i-1)*f->getYCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                                //cout <<"convert "<<x<<"-"<<aX<<"   ";
				u1[posNewFrame] = (((x-aX)/factorU)*factorU) +aX;
				buffer[pos]=convert( (x-aX)/factorU);
				posNewFrame++;
				pos++;
			}
		}
		posNewFrame = 0;		//reset component index of new frame
		/* Calculating values of V component of original frame */
		for (int i = 0; i < f->getZRows(); i++) {
            for (int j = 0; j < f->getZCols(); j++) { 
				x=v[i*f->getZCols()+j];				 
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=v1[i*f->getZCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=v1[(i-1)*f->getZCols()+j];
					c=0;
				}
				else{
					a=v1[i*f->getZCols()+j-1];
					b=v1[(i-1)*f->getZCols()+j];
					c=v1[(i-1)*f->getZCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                                //cout <<"convert "<<x<<"-"<<aX<<"   ";
				v1[posNewFrame] = (((x-aX)/factorV)*factorV) +aX;
				buffer[pos]=convert( (x-aX)/factorV);
				posNewFrame++;
				pos++;
			}
		}
      
      
    }else{
	for (int i = 0; i < f->getXRows(); i++) {
            for (int j = 0; j < f->getXCols(); j++) { 
            	//cout << "(" << i << ", " << j << ")" << endl;
				x=y[i*f->getXCols()+j];				 
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=y[i*f->getXCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=y[(i-1)*f->getXCols()+j];
					c=0;
				}
				else{
					a=y[i*f->getXCols()+j-1];
					b=y[(i-1)*f->getXCols()+j];
					c=y[(i-1)*f->getXCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                                //cout <<"convert "<<x<<"-"<<aX<<"   ";
				buffer[pos]=convert(x-aX);
				pos++;
			}
		}
		
		for (int i = 0; i < f->getYRows(); i++) {
            for (int j = 0; j < f->getYCols(); j++) { 
				x=u[i*f->getYCols()+j];				 
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=u[i*f->getYCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=u[(i-1)*f->getYCols()+j];
					c=0;
				}
				else{
					a=u[i*f->getYCols()+j-1];
					b=u[(i-1)*f->getYCols()+j];
					c=u[(i-1)*f->getYCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                                //cout <<"convert "<<x<<"-"<<aX<<"   ";
				buffer[pos]=convert(x-aX);
				pos++;
			}
		}
		
		for (int i = 0; i < f->getZRows(); i++) {
            for (int j = 0; j < f->getZCols(); j++) { 
				x=v[i*f->getZCols()+j];				 
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=v[i*f->getZCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=v[(i-1)*f->getZCols()+j];
					c=0;
				}
				else{
					a=v[i*f->getZCols()+j-1];
					b=v[(i-1)*f->getZCols()+j];
					c=v[(i-1)*f->getZCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                                //cout <<"convert "<<x<<"-"<<aX<<"   ";
				buffer[pos]=convert(x-aX);
				pos++;
			}
		}
    }
				
}

void JpegLs::decode(unsigned short *buffer, Frame *f){
	unsigned char *y;
	unsigned char *u;
	unsigned char *v;

	int i,j;
	pos=0;
	
	f->getChannels(&y,&u,&v);	

	if(lossy){
      /* Decoding values of Y component */
      for(i=0; i<f->getXRows(); i++){
			for(j=0; j<f->getXCols(); j++){
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=y[i*f->getXCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=y[(i-1)*f->getXCols()+j];
					c=0;
				}
				else{
					a=y[i*f->getXCols()+j-1];
					b=y[(i-1)*f->getXCols()+j];
					c=y[(i-1)*f->getXCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;				

                            y[i*f->getXCols()+j]=(unsigned char) ( unconvert( buffer[pos]) * factorY + aX) ;
                            pos++;
			}
		}
		/* Decoding values of U component */
		for(i=0; i<f->getYRows(); i++){
			for(j=0; j<f->getYCols(); j++){
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=u[i*f->getYCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=u[(i-1)*f->getYCols()+j];
					c=0;
				}
				else{
					a=u[i*f->getYCols()+j-1];
					b=u[(i-1)*f->getYCols()+j];
					c=u[(i-1)*f->getYCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;

                            u[i*f->getYCols()+j]=(unsigned char)( unconvert( buffer[pos]) * factorU + aX) ;
                            pos++;
			}
		}
		/* Decoding values of V component */
		for(i=0; i<f->getZRows(); i++){
			for(j=0; j<f->getZCols(); j++){
				
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=v[i*f->getZCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=v[(i-1)*f->getZCols()+j];
					c=0;
				}
				else{
					a=v[i*f->getZCols()+j-1];
					b=v[(i-1)*f->getZCols()+j];
					c=v[(i-1)*f->getZCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;

                            v[i*f->getZCols()+j]=(unsigned char)( unconvert( buffer[pos]) * factorV + aX) ;
                            pos++;
			}
		}
      
      
    }else{
		for(i=0; i<f->getXRows(); i++){
			for(j=0; j<f->getXCols(); j++){
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=y[i*f->getXCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=y[(i-1)*f->getXCols()+j];
					c=0;
				}
				else{
					a=y[i*f->getXCols()+j-1];
					b=y[(i-1)*f->getXCols()+j];
					c=y[(i-1)*f->getXCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;				
					
                            //cout <<"unconvert "<<buffer[pos]<<"   ";
                            //cout <<"aX: "<<aX<<endl;
                            //cout << "pixel: "<<(unconvert(buffer[pos])+aX)<<endl;
                            y[i*f->getXCols()+j]=(unsigned char)(unconvert(buffer[pos])+aX);
                            pos++;
			}
		}
		
		for(i=0; i<f->getYRows(); i++){
			for(j=0; j<f->getYCols(); j++){
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=u[i*f->getYCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=u[(i-1)*f->getYCols()+j];
					c=0;
				}
				else{
					a=u[i*f->getYCols()+j-1];
					b=u[(i-1)*f->getYCols()+j];
					c=u[(i-1)*f->getYCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                            //cout <<"unconvert "<<buffer[pos]<<"   ";
                            //cout <<"aX: "<<aX<<endl;
                            //cout << "pixel: "<<(unconvert(buffer[pos])+aX)<<endl;
                            u[i*f->getYCols()+j]=(unsigned char)(unconvert(buffer[pos])+aX);
                            pos++;
			}
		}
		
		for(i=0; i<f->getZRows(); i++){
			for(j=0; j<f->getZCols(); j++){
				
				if(j==0 && i==0){
					a=0;
					b=0;
					c=0;					
				}
				else if(i==0){
					a=v[i*f->getZCols()+j-1];
					b=0;
					c=0;
				}
				else if(j==0){
					a=0;
					b=v[(i-1)*f->getZCols()+j];
					c=0;
				}
				else{
					a=v[i*f->getZCols()+j-1];
					b=v[(i-1)*f->getZCols()+j];
					c=v[(i-1)*f->getZCols()+j-1];
				}
				
				if (c >= max(a,b))
					aX=min(a,b);
				else if (c <= min(a,b))
					aX=max(a,b);
				else
					aX=a+b-c;
                            //cout <<"unconvert "<<buffer[pos]<<"   ";
                            //cout <<"aX: "<<aX<<endl;
                            //cout << "pixel: "<<(unconvert(buffer[pos])+aX)<<endl;
                            v[i*f->getZCols()+j]=(unsigned char)(unconvert(buffer[pos])+aX);
                            pos++;
			}
		}
	}
}

unsigned char JpegLs::getSignal(short a){
	return a<0 ? 1 : 0;
}

unsigned short JpegLs::getModule(short a){
	return a>=0 ? a : -a;
}

short JpegLs::min(short a, short b){
	return a<b ? a:b;
}

short JpegLs::max(short a, short b){
	return a>b ? a:b;
}

unsigned short JpegLs::convert(short in){
	unsigned char signal;
	unsigned short out;
	
	signal=getSignal(in);
	out= 0x00FF & getModule(in);
        
        //cout <<"signal: "<<(unsigned int) signal<<" | value: "<<out<<endl;
        
	bitwrapper bw((unsigned char *) &out, sizeof(short));
	bw.write(8,signal);
	return out;
}

short JpegLs::unconvert(unsigned short in){
	unsigned char signal;
	short out=0x00FF & in;
	
	bitwrapper bw((unsigned char *) &in, sizeof(unsigned short));
	signal=bw.read(8);
        
	if(signal){
		//cout <<"signal: "<<(unsigned int) signal<<" | value: "<<out<<endl;
	    return -out;
	}
	//cout <<"signal: "<<(unsigned int) signal<<" | value: "<<out<<endl;
	return out;
        
	//return out;
}

