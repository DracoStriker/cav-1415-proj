/* 
 * File:   golombDecoder.cpp
 * Author: nuno
 * 
 * Created on November 15, 2014, 4:16 PM
 */

#include "GolombRiceDecoderAudio.hpp"

using namespace std;


GolombRiceDecoderAudio::GolombRiceDecoderAudio(unsigned int mParam, istream *is){
    
        /* Initialization of variables and buffer */
    m = mParam;
    bw_index = 0;
    maxBufferSize = ((255/m + log2(m) + 1) / 8) + 1;
    
    /* Calculating constants */
    b = log2(m)+1;
    l = (pow(2, b) - m);

    buf = new unsigned char [maxBufferSize];
    
    /* Creation of bitwrapper */
    bitWrapper = new bitwrapper(buf, maxBufferSize);
    
    /* Creation of ibitstream */
    ibs = new ibitstream(is);

    cout << "Golomb Decoder Audio" << endl;
    
}

/*golombDecoder::golombDecoder(const golombDecoder& orig) {
}*/

GolombRiceDecoderAudio::~GolombRiceDecoderAudio() {
    //delete ibs; 
    //delete bitWrapper;
    //delete[] buf;
}

unsigned int GolombRiceDecoderAudio::decoder(){
    
    /* Resetting values */
    aux = bw_index = quotient = remainder = 0;
    
    /* Get the value of quotient - read ones until 0 appears */
    ibs->read(*bitWrapper,1);

    while( bitWrapper->read(bw_index) ){
        quotient++;
        ibs->read(*bitWrapper,1);
    }

    
    //cout << "Positive: " << positive << " Quotient: " << quotient; //<< " b: " << b <<  endl;

    /* Remainder decoding */
    
    /* Read b-1 bits to bitWrapper */
    ibs->read(*bitWrapper,b-1);
    
    /* Go through bitWrapper and obtain the value of the remainder in unsigned short*/
    for(int i = 0; i < (b-1); i++){
        bit = (bitWrapper->read(bw_index++)) ? 1 : 0;
        aux |= bit;
        if(i != b-2)    //don't shift at last bit unless you'll need it
            aux = aux << 1;
        
    }

    
    if(aux < l)
        remainder = aux;
    else{
	
        bw_index = 0;                   //reset index of bitWrapper
        ibs->read(*bitWrapper,1);       //read 1 more bit
        aux = aux << 1;                 //shift val to insert one more bit
        aux |= bitWrapper->read(bw_index++);
        
        remainder = aux - l;
    }
    //cout << " Remainder: " << remainder << endl;
    
    unsigned int res;
    res = quotient*m + remainder;        //calculate end result
    //cout << "Value of modulus of codeword: " << res << endl;
    
    if(res != 0){
    
    /* Reading signal bit */
    	ibs->read(*bitWrapper,1);
    	
    	/* Get the signal of the value */
    	positive = ( bitWrapper->read(0) ) ? false : true;
    }else
      positive = 1;
      
    
    /* Add value sign */
    if(!positive)
        res |= 0x10000;
    
    return res;
    
}


