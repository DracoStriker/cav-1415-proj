/* 
 * File:   golombDecoder.hpp
 * Author: nuno
 *
 * Created on November 15, 2014, 4:16 PM
 */

#ifndef GOLOMBRICEDECODER_HPP
#define	GOLOMBRICEDECODER_HPP

#include "GolombRiceModule.hpp"

class GolombRiceDecoder : public GolombRiceModule{
public:

    GolombRiceDecoder(unsigned int m, istream *is);
    //golombDecoder(const golombDecoder& orig);
    virtual ~GolombRiceDecoder();
    
    /**
     * Uses the M parameter obtained from file and uses the pointer to the stream, in order to read the necessary bits and return the value decoded
     * in a (Sign+Module) way
     * @return Value of a byte in a (Sign+Module) way
     */
    unsigned short decoder();
    
    
protected:
    
    /* Properties */
    /** Pointer to ibitstream in order to read the bits of a file */
    ibitstream *ibs;

};

#endif	/* GOLOMBRICEDECODER_HPP */

