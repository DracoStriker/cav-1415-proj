/* 
 * File:   JpegLs.h
 * Author: pedro
 *
 * Created on November 11, 2014, 5:17 PM
 */

#ifndef JPEGLS_H
#define	JPEGLS_H

#include "Frame.hpp"

class JpegLs {
public:
    JpegLs();
    JpegLs(int lossy, int factorY, int factorU, int factorZ);
    virtual ~JpegLs();
    void encode(Frame *f, unsigned short *buffer);
    void decode(unsigned short *buffer, Frame *f);
    static unsigned short convert(short in);
    static short unconvert(unsigned short in);
private:
	short a;
        short b;
        short c;
        short x;
        short aX;
        int pos;
	/** Flag that indicates if using lossy or lossless intra-frame compression */
	int lossy;
	/** Index of the auxiliary frame in the specified component*/
	int posNewFrame;
	/** Factor by which we divide the predictor value in order to allow lossy compression */
	int factorY, factorU, factorV;
	static unsigned char getSignal(short a);
	static unsigned short getModule(short a);
	short min(short a, short b);
	short max(short a, short b);
};

#endif	/* JPEGLS_H */
