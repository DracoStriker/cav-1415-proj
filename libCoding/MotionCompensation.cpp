/* 
 * File:   MotionCompensation.cpp
 * Author: simon
 * 
 * Created on November 18, 2014, 6:07 PM
 */

#include <limits>

#include "MotionCompensation.hpp"
#include "JpegLs.hpp"

MotionCompensation::MotionCompensation() : Delta(2), blk(), prevBlk(), lossy(false), factorY(1), factorU(1), factorV(1) {
}

MotionCompensation::MotionCompensation(unsigned int Delta) : Delta(Delta), blk(), prevBlk(), lossy(false), factorY(1), factorU(1), factorV(1) {
}

MotionCompensation::MotionCompensation(unsigned int nRows, unsigned int nCols) : Delta(2), blk(nRows, nCols), prevBlk(nRows, nCols), lossy(false), factorY(1), factorU(1), factorV(1) {
}

MotionCompensation::MotionCompensation(unsigned int Delta, unsigned int nRows, unsigned int nCols) : Delta(Delta), blk(nRows, nCols), prevBlk(nRows, nCols), lossy(false), factorY(1), factorU(1), factorV(1) {
}

MotionCompensation::MotionCompensation(unsigned int Delta, unsigned int nRows, unsigned int nCols, unsigned int factorY, unsigned int factorU, unsigned int factorV) : Delta(Delta), blk(nRows, nCols), prevBlk(nRows, nCols), lossy(true), factorY(factorY), factorU(factorU), factorV(factorV) {
}

MotionCompensation::~MotionCompensation() {
}

void MotionCompensation::encode(Frame *previous, Frame *frame, unsigned short *deltas) {
	double minDist;
	double distance;
	unsigned int idx = 0;
	int x, y;
	int nBlockRows, nBlockCols;
	for (unsigned int ch = 1; ch <= 3; ch++) {
		switch (ch) {
			case 1:
				nBlockRows = frame->getXBlockRows(blk);
				nBlockCols = frame->getXBlockCols(blk);
				break;
			case 2:
				nBlockRows = frame->getYBlockRows(blk);
				nBlockCols = frame->getYBlockCols(blk);
				break;
			case 3:
				nBlockRows = frame->getZBlockRows(blk);
				nBlockCols = frame->getZBlockCols(blk);
				break;
		}
		for (unsigned int r = 0; r < nBlockRows; r++) {
			for (unsigned int c = 0; c < nBlockCols; c++) {
				frame->getExclusiveBlockVal((int) r, (int) c, &blk, (int) ch);
				setSearchLimits(*frame, blk, r, c, ch);
				minDist = numeric_limits<double>::max();
				//prevBlk.setRows(blk.getRows());
				//prevBlk.setCols(blk.getCols());
				//cout << "[" << c << ", " << r << "]" << endl;
				//cout << "(" << blk.getCols() << ", " << blk.getRows() << ")" << endl;
				for (int pr = minDeltaY; pr <= maxDeltaY; pr++) {
					for (int pc = minDeltaX; pc <= maxDeltaX; pc++) {
						//cout << "(" << pc << ", " << pr << ")" << endl;
						previous->getBlockVal(pr, pc, &prevBlk, (int) ch);
						//cout << "<" << prevBlk.getCols() << ", " << prevBlk.getRows() << ">" << endl;
						distance = blk.distance(prevBlk);
						//cout << " d = " << distance << endl;
						if (distance < minDist) {
							minDist = distance;
							x = pc - blk.getXPos();
							y = pr - blk.getYPos();
						}
					}
				}
				//cout << "min = " << minDist << endl;
				//cout << "<" << (blk.getYPos() - y) << ", " << (blk.getXPos() - x) << ">" << endl;
				deltas[idx] = JpegLs::convert((short) x);
				idx++;
				//cout << "x = " << x << endl;
				deltas[idx] = JpegLs::convert((short) y);
				idx++;
				//cout << "y = " << y << endl;
				previous->getBlockVal(blk.getYPos() + y, blk.getXPos() + x, &prevBlk, (int) ch);
				for (unsigned int ir = 0; ir < blk.getRows(); ir++) {
					for (unsigned int ic = 0; ic < blk.getCols(); ic++) {
						//cout << "|" << ic << ", " << ir << "|" << endl;
						//cout << (short)(blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)) << " = " << (short)(blk.getValue(ir, ic)) << " - " << (short)prevBlk.getValue(ir, ic) << endl;
						if (!lossy)
							deltas[idx] = JpegLs::convert((short)(blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)));
						else switch (ch) {
							case 1:
								deltas[idx] = JpegLs::convert(((short)(blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)))/factorY);
								blk.setValue(ir, ic, ((blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)) / factorY) * factorY + prevBlk.getValue(ir, ic));
								break;
							case 2:
								deltas[idx] = JpegLs::convert(((short)(blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)))/factorU);
								blk.setValue(ir, ic, ((blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)) / factorU) * factorU + prevBlk.getValue(ir, ic));
								break;
							case 3:
								deltas[idx] = JpegLs::convert(((short)(blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)))/factorV);
								blk.setValue(ir, ic, ((blk.getValue(ir, ic) - prevBlk.getValue(ir, ic)) / factorV) * factorV + prevBlk.getValue(ir, ic));
								break;
						}
						idx++;
					}
				}
				frame->setExclusiveBlockVal(blk, (int) r, (int) c, (int) ch);
			}
			//string input = "";
			//getline(cin, input);
		}
	}
	//cout << "   " << idx << endl;
}

void MotionCompensation::decode(Frame *previous, unsigned short *deltas, Frame *frame) {
	unsigned int idx = 0;
	int x, y;
	int nBlockRows, nBlockCols;
	unsigned int pr, pc;
	for (unsigned int ch = 1; ch <= 3; ch++) {
		switch (ch) {
			case 1:
				nBlockRows = frame->getXBlockRows(blk);
				nBlockCols = frame->getXBlockCols(blk);
				break;
			case 2:
				nBlockRows = frame->getYBlockRows(blk);
				nBlockCols = frame->getYBlockCols(blk);
				break;
			case 3:
				nBlockRows = frame->getZBlockRows(blk);
				nBlockCols = frame->getZBlockCols(blk);
				break;
		}
		for (unsigned int r = 0; r < nBlockRows; r++) {
			for (unsigned int c = 0; c < nBlockCols; c++) {
				x = JpegLs::unconvert(deltas[idx]);
				idx++;
				//cout << "x = " << x << endl;
				y = JpegLs::unconvert(deltas[idx]);
				idx++;
				//cout << "y = " << y << endl;
				frame->checkExclusiveBlockPos(r, c, &blk, ch);
				//cout << "[" << c << ", " << r << "]" << endl;
				//cout << "(" << blk.getCols() << ", " << blk.getRows() << ")" << endl;
				pc = blk.getXPos() + x;
				pr = blk.getYPos() + y;
				//cout << "pc = " << pc << endl;
				//cout << "pr = " << pr << endl;
				previous->getBlockVal((int) pr, (int) pc, &prevBlk, (int) ch);
				for (unsigned int ir = 0; ir < blk.getRows(); ir++) {
					for (unsigned int ic = 0; ic < blk.getCols(); ic++) {
						//cout << "|" << ic << ", " << ir << "|" << endl;
						//cout << (short)(JpegLs::unconvert(deltas[idx]) + prevBlk.getValue(ir, ic)) << " = " << JpegLs::unconvert(deltas[idx]) << " + " << (short)prevBlk.getValue(ir, ic) << endl;
						if (!lossy)
							blk.setValue(ir, ic, (short)(JpegLs::unconvert(deltas[idx]) + prevBlk.getValue(ir, ic)));
						else switch (ch) {
							case 1:
								blk.setValue(ir, ic, (short)(JpegLs::unconvert(deltas[idx]) * factorY + prevBlk.getValue(ir, ic)));
								break;
							case 2:
								blk.setValue(ir, ic, (short)(JpegLs::unconvert(deltas[idx]) * factorU + prevBlk.getValue(ir, ic)));
								break;
							case 3:
								blk.setValue(ir, ic, (short)(JpegLs::unconvert(deltas[idx]) * factorV + prevBlk.getValue(ir, ic)));
								break;
						}
						idx++;
					}
				}
				frame->setExclusiveBlockVal(blk, (int) r, (int) c, (int) ch);
			}
			//string input = "";
			//getline(cin, input);
		}
	}
	//cout << "   " << idx << endl;
}

void MotionCompensation::setSearchLimits(Frame &f, Block &b, unsigned int r, unsigned int c, unsigned int ch) {
	minDeltaX = b.getXPos() - Delta;
	minDeltaX = (minDeltaX < 0) ? 0 : minDeltaX;
	minDeltaY = b.getYPos() - Delta;
	minDeltaY = (minDeltaY < 0) ? 0 : minDeltaY;
	switch (ch) {
	case 1:
		maxDeltaX = b.getXPos() + Delta;
		maxDeltaX = (maxDeltaX + b.getCols() >= f.getXCols()) ? f.getXCols() - b.getCols() : maxDeltaX;
		maxDeltaY = b.getYPos() + Delta;
		maxDeltaY = (maxDeltaY + b.getRows() >= f.getXRows()) ? f.getXRows() - b.getRows() : maxDeltaY;
		break;
	case 2:
		maxDeltaX = b.getXPos() + Delta;
		maxDeltaX = (maxDeltaX + b.getCols() >= f.getYCols()) ? f.getYCols() - b.getCols() : maxDeltaX;
		maxDeltaY = b.getYPos() + Delta;
		maxDeltaY = (maxDeltaY + b.getRows() >= f.getYRows()) ? f.getYRows() - b.getRows() : maxDeltaY;
		break;
	case 3:
		maxDeltaX = b.getXPos() + Delta;
		maxDeltaX = (maxDeltaX + b.getCols() >= f.getZCols()) ? f.getZCols() - b.getCols() : maxDeltaX;
		maxDeltaY = b.getYPos() + Delta;
		maxDeltaY = (maxDeltaY + b.getRows() >= f.getZRows()) ? f.getZRows() - b.getRows() : maxDeltaY;
		break;
	}
	//cout << "(" << blk.getXPos() << ", " << blk.getYPos() << ")" << endl;
	//cout << " minx = " << minDeltaX << " maxx = " << maxDeltaX << " miny = " << minDeltaY << " maxy = " << maxDeltaY << endl;
}
