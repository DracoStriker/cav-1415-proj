/* 
 * File:   InterChannelAudioPredictor.h
 * Author: nuno
 *
 * Created on Dezember 13, 2014, 17:30 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "IntraChannelAudioPredictor.hpp"

#ifndef ADVANCED_CHANNEL_AUDIO_PREDICTOR_H
#define	ADVANCED_CHANNEL_AUDIO_PREDICTOR_H

#define N_INTRA_PREDICTORS 5
#define N_INTER_PREDICTORS 1
#define N_PREDICTORS (N_INTRA_PREDICTORS + N_INTER_PREDICTORS)

class AdvancedChannelAudioPredictor {
public:
    AdvancedChannelAudioPredictor(int, int);
    virtual ~AdvancedChannelAudioPredictor();


    /**
     *	Fill array with original values of a given block for each channel
     * @param pointer to the array of the original values
     * @param channel
     *
     */
    void fillBlock(short*, int);


    /**
     *	 Calculates the best predictor for the given block
     *
     *	@param channel
     * 	@return The id of the linear expression that causes less entropy
     */
    int encode(int channel);

    /**
     *	Returns the best linear expression for the given block
     *
     * 	@return The pointer to an array with the values of the residuals of a block, coded with the best linear expression for that block
     */
    int* getBestPredVals();

    /**
     *	Decodes the value received for the channel using the linear expression also specified as parameter
     *	@param val - Residual value of the given channel 
     *	@param other - Residual value of the other channel 
     *	@param order - Linear expression to be applied to the block in order to decode it correctly
     *	@param history - Pointer to an array with last 3 sample values (pastSamples)
     *	@param blockIndex - index of the block
     * 	@param channel
     *
     *	@return Original value, ie value decoded
     */
    short decode(int val, int other, int order, int* history, int blockIndex, int channel);

    /** ID of the best linear expression, on channel0, for the given block */
	//int ch0Best;

private:
	/* Methods */
	/* */

	/**
	 * Applies the different linear expressions, calculates and saves the values returning the number of linear expression with min values (less entropy)
	 *
	 * 	@param order - ID of Linear expression to be applied
	 *	@param blk_index - Block index
	 *
	 *	@return ID of best linear expression for the given block
	 */
	int calcOrderExpression(int order, int blk_index);

	/**
	 *	Calculate min value and increment its counter. Counter is the number of blocks where the given predictor was the best prediction
	 *
	 *	@param Array with counter vals of the residuals of each prediction (linear expression)
	 *
	 */
	void calcMinAndIncrement(int[]);

	/**
	 *	Adds the residuals to the appropriate counter. Counter is the sum of the residuals.
	 *	@param Array with the values of the residuals of each prediction (linear expression) to be added to the counter
	 *
	 */
	void addAbsResiduals(int[]);

	/**
	 *	Calculates the minimum value of the residuals
	 *
	 *	@param Array with the residual vals calculated
	 *	@param Number of predictors
	 *
	 *	@return The ID of the best predictor
	 *
	 */
	int calcMinVal(int[], int);

	/* Calculates the max value */
	//int calcMaxVal(int[], int);

	/**
	 *	 Calculates the module of the value
	 *
	 * @param value 
	 *
	 * @return The absolute value of the input value
	 */
	int myAbs(int);

	/* Properties */
	/** Channel the predictions are being made */
	int ch;
	/** Number of channels */
	int nCh;
	/** Block size */
	int blockSize;
	/** Counters of the best linear expression */
	int contOrd[N_PREDICTORS][2];
	/** Value from linear expression */
	int valOrd[N_PREDICTORS][2];
	/** Values obtained from linear predictions */
	int **predVals[N_PREDICTORS]; 
	/** History of the last values */
	int *pastSamples;
	/** ID of the best linear expression, on channel0, for the given block */
	int ch0Best;
};

#endif	/* ADVANCED_CHANNEL_AUDIO_PREDICTOR_H */