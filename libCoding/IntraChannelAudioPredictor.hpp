/* 
 * File:   IntraChannelAudioPredictor.h
 * Author: simon
 *
 * Created on Dezember 13, 2014, 17:30 PM
 */

#ifndef INTRA_CHANNEL_AUDIO_PREDICTOR_H
#define	INTRA_CHANNEL_AUDIO_PREDICTOR_H

class IntraChannelAudioPredictor {
public:
    IntraChannelAudioPredictor();
    virtual ~IntraChannelAudioPredictor();
    unsigned int encode(short, unsigned int);
    short decode(unsigned int, unsigned int);
    static unsigned int convert(int);
    static int unconvert(unsigned int);
    int getDelta();
        
private:
    short previous[2];
    int delta;
    static unsigned char getSignal(int);
    static unsigned int getModule(int);
    static int min(int, int);
    static int max(int, int);
};

#endif	/* INTRA_CHANNEL_AUDIO_PREDICTOR_H */
