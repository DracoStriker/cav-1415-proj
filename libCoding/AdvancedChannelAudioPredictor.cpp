
#include "AdvancedChannelAudioPredictor.hpp"
#include <cstring>
#include <climits>

using namespace std;

AdvancedChannelAudioPredictor::AdvancedChannelAudioPredictor(int blkSize, int numCh) {
	/* Initialize properties */
	nCh = numCh;
	blockSize = blkSize;

	/* Reset counters */
	for(int i = 0; i < N_PREDICTORS; i++)
		for(int j = 0; j< nCh; j++)
			contOrd[i][j] = 0;
	
	/* Initialize array with predicted vals for each expression */
	for(int i = 0; i < N_PREDICTORS; i++)
		predVals[i] = new int*[nCh];

	for(int i = 0; i < N_PREDICTORS; i++)
		for(int j = 0; j < nCh; j++)
			predVals[i][j] = new int[blockSize];
}


AdvancedChannelAudioPredictor::~AdvancedChannelAudioPredictor() {
}


short AdvancedChannelAudioPredictor::decode(int val, int other, int order, int* history, int blockIndex, int channel){

	/* Last samples pointer */
	pastSamples=history;

	/* Choose linear expression to be used */
	switch(order){
		case 0:
			return val;
		case 1:
			if(blockIndex == 0)
				return val;
			else
				return val + pastSamples[N_INTRA_PREDICTORS-2];					//e(n) + y (n-1) = y(n)
		case 2:
			if(blockIndex == 0)
				return val;
			else if (blockIndex == 1)
				return val + (pastSamples[N_INTRA_PREDICTORS-2] << 1);
			else
				return val + (pastSamples[N_INTRA_PREDICTORS-2] << 1) - pastSamples[N_INTRA_PREDICTORS-3];			//e(n) + 2y(n-1) - y(n-2) = y(n)
		case 3:
			if(blockIndex == 0)
				return val;
			else if (blockIndex == 1)
				return val + 3 * pastSamples[N_INTRA_PREDICTORS-2];
			else if (blockIndex == 2)
				return val + 3 * pastSamples[N_INTRA_PREDICTORS-2] - 3 * pastSamples[N_INTRA_PREDICTORS-3];
			else
				return val + 3 * pastSamples[N_INTRA_PREDICTORS-2] - 3 * pastSamples[N_INTRA_PREDICTORS-3] + pastSamples[N_INTRA_PREDICTORS-4];			//e(n) + 3y(n-1) -3y(n-2) + y(n-3) = y(n)
		case 4:
			if(blockIndex == 0)
				return val;
			else if (blockIndex == 1)
				return val + 4 * pastSamples[N_INTRA_PREDICTORS-2];
			else if (blockIndex == 2)
				return val + 4 * pastSamples[N_INTRA_PREDICTORS-2] - 6 * pastSamples[N_INTRA_PREDICTORS-3];
			else if (blockIndex == 3)
				return val + 4 * pastSamples[N_INTRA_PREDICTORS-2] - 6 * pastSamples[N_INTRA_PREDICTORS-3] + 4 * pastSamples[N_INTRA_PREDICTORS-4];
			else
				return val + 4 * pastSamples[N_INTRA_PREDICTORS-2] - 6 * pastSamples[N_INTRA_PREDICTORS-3] + 4 * pastSamples[N_INTRA_PREDICTORS-4] - pastSamples[N_INTRA_PREDICTORS-5];	//e(n) + 4y(n-1) -6y(n-2) + 4y(n-3) -y(n-4) = y(n)
		case 5:
			if (channel == 0)
				return (short)((other + val * 2) / 2); // l = (s + 2m) / 2;  
			else
				return (short)((other * 2 - val) / 2); // r = (2m - s) / 2;
		default:
			cout << "Expression order not available" << endl;
			return -1;
	}

}

void AdvancedChannelAudioPredictor::fillBlock(short* block, int channel) {

	ch = channel;

	/* Reset the counters */
	for(int i = 0; i < N_PREDICTORS; i++)
		contOrd[i][ch] = 0;
	
	/* Initialize the internal array with the original values */
	for(int b = 0; b<blockSize; b++)
		predVals[0][ch][b] = (int) block[b];
}

int AdvancedChannelAudioPredictor::encode(int channel) {

	ch = channel;

	for(int i = 0; i < blockSize; i++){
		
		/* Calculation of the residuals for each linear expression */
		for(int j = 0; j < N_PREDICTORS; j++)
			valOrd[j][ch] = calcOrderExpression( j, i);


		//calcMinAndIncrement( myAbs(valOrd0[ch]), myAbs(valOrd1[ch]), myAbs(valOrd2[ch]), myAbs(valOrd3[ch]) );

		int valOrdCh[N_PREDICTORS];	//aux variable

		/* Get the module of each residual val */
		for (int i = 0; i < N_PREDICTORS; i++)
			valOrdCh[i] = myAbs(valOrd[i][ch]);	

		/* Add the residual values calculated to its counter */
		addAbsResiduals(valOrdCh);
	}

	int nPredictors;

	if ((channel == 1) && (ch0Best == 5))
		return 5;
	else if ((channel == 1) && (ch0Best != 5))
		nPredictors = N_INTRA_PREDICTORS;
	else
		nPredictors = N_PREDICTORS;

	/* return the value of the best predictor order */
	int contOrdCh[N_PREDICTORS];
	for (int i = 0; i < nPredictors; i++)
		contOrdCh[i] = contOrd[i][ch];

	/* Return the ID of the best linear expression for the given block */
	return calcMinVal(contOrdCh, nPredictors);
}

int* AdvancedChannelAudioPredictor::getBestPredVals() {
	int contOrdCh[N_PREDICTORS];

	if (ch == 0) {
		for (int i = 0; i < N_PREDICTORS; i++)
			contOrdCh[i] = contOrd[i][ch];
		ch0Best = calcMinVal(contOrdCh, N_PREDICTORS);		//return of id of the best linear expression for the given block

		return predVals[ch0Best][ch];
	} else {
		/* Return vals of prediction 5 on channel 1, if that prediction is best on channel 0 */
		if (ch0Best == 5) 
			return predVals[5][ch];
		/* Returns vals of best prediction*/
		else{
			for (int i = 0; i < N_INTRA_PREDICTORS; i++)
				contOrdCh[i] = contOrd[i][ch];
			return predVals[calcMinVal(contOrdCh, N_INTRA_PREDICTORS)][ch];
		}
	}
}

int AdvancedChannelAudioPredictor::calcOrderExpression(int order, int blk_index) {
	/* Calculation of the residuals for each linear expression */
	switch(order) {
		case 0:
			return predVals[0][ch][blk_index];
		case 1:
			if (blk_index == 0)
				predVals[1][ch][blk_index] = 0;
			else
				predVals[1][ch][blk_index] = predVals[0][ch][blk_index-1];
			return (predVals[1][ch][blk_index] = predVals[0][ch][blk_index] - predVals[1][ch][blk_index]);				//e(n) = y(n) - y(n-1)
		case 2:
			if (blk_index == 0)
				predVals[2][ch][blk_index] = 0;
			else if (blk_index == 1)
				predVals[2][ch][blk_index] = 2 * predVals[0][ch][blk_index-1];
			else
				predVals[2][ch][blk_index] = 2 * predVals[0][ch][blk_index-1] - predVals[0][ch][blk_index-2];
			return (predVals[2][ch][blk_index] = predVals[0][ch][blk_index] - predVals[2][ch][blk_index]);				//e(n) = y(n) - 2y(n-1) + y(n-2)
		case 3:
			if (blk_index == 0)
				predVals[3][ch][blk_index] = 0;
			else if (blk_index == 1)
				predVals[3][ch][blk_index] = 3 * predVals[0][ch][blk_index-1];
			else if (blk_index == 2)
				predVals[3][ch][blk_index] = 3 * predVals[0][ch][blk_index-1] - 3 * predVals[0][ch][blk_index-2] ;
			else
				predVals[3][ch][blk_index] = 3 * predVals[0][ch][blk_index-1] - 3 * predVals[0][ch][blk_index-2] + predVals[0][ch][blk_index-3];
			return (predVals[3][ch][blk_index] = predVals[0][ch][blk_index] - predVals[3][ch][blk_index]);				//e(n) = y(n) -3y(n-1) + 3y(n-2) -y(n-3)
		case 4:
			if (blk_index == 0)
				predVals[4][ch][blk_index] = 0;
			else if (blk_index == 1)
				predVals[4][ch][blk_index] = 4 * predVals[0][ch][blk_index-1];
			else if (blk_index == 2)
				predVals[4][ch][blk_index] = 4 * predVals[0][ch][blk_index-1] - 6 * predVals[0][ch][blk_index-2];
			else if (blk_index == 3)
				predVals[4][ch][blk_index] = 4 * predVals[0][ch][blk_index-1] - 6 * predVals[0][ch][blk_index-2] + 4 * predVals[0][ch][blk_index-3];
			else
				predVals[4][ch][blk_index] = 4 * predVals[0][ch][blk_index-1] - 6 * predVals[0][ch][blk_index-2] + 4 * predVals[0][ch][blk_index-3] - predVals[0][ch][blk_index-4];
			return (predVals[4][ch][blk_index] = predVals[0][ch][blk_index] - predVals[4][ch][blk_index]);				//e(n) = y(n) -4y(n-1) + 6y(n-2) -4y(n-3) +y(n-4)
		case 5:
			if (ch == 0)
				return (predVals[5][0][blk_index] = (predVals[0][0][blk_index] + predVals[0][1][blk_index]) / 2);	// e = (l + r) / 2;
			else
				return (predVals[5][1][blk_index] = predVals[0][0][blk_index] - predVals[0][1][blk_index]); // e = l - r;
			break;
		default:
			cout << "Expression order not available" << endl;
			return -1;
	}
}

void AdvancedChannelAudioPredictor::addAbsResiduals(int val[]){
	/* Adds the values into respective counter*/
	for (int i = 0; i < N_PREDICTORS; i++)
		contOrd[i][ch]+=val[i];
}

void AdvancedChannelAudioPredictor::calcMinAndIncrement(int val[]){
	/* Adds the values into respective counter*/
	contOrd[calcMinVal(val, N_PREDICTORS)][ch]++;
}

int AdvancedChannelAudioPredictor::calcMinVal(int val[], int N){
	int min = INT_MAX;
	int idxMin;
	for (int i = 0; i < N; i++)
		if (val[i] < min) {
			min = val[i];
			idxMin = i;
		}
	return idxMin;
}

/*int AdvancedChannelAudioPredictor::calcMaxVal(int val[], int N) {
	int max = INT_MIN;
	int idxMax;
	for (int i = 0; i < N; i++)
		if (val[i] > max) { 
			max = val[i];
			idxMax = i;
		}
	return idxMax;
}*/

int AdvancedChannelAudioPredictor::myAbs(int val){
	return val < 0 ? -val : val;
}

