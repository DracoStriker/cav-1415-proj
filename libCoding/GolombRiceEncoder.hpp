/* 
 * File:   golombEncoder.hpp
 * Author: nuno
 *
 * Created on November 15, 2014, 4:15 PM
 */

#ifndef GOLOMBRICEENCODER_HPP
#define	GOLOMBRICEENCODER_HPP

#include "GolombRiceModule.hpp"

class GolombRiceEncoder : public GolombRiceModule{
public:
    
    GolombRiceEncoder(unsigned int m, ostream* os);
    //golombEncoder(const golombEncoder& orig);
    virtual ~GolombRiceEncoder();
    
    /**
     * Encodes the value using GolombRice method and writes the contents of bitWrapper directly to file.
     * @param pred Value of the prediction obtained (Sign+Module)
     * @return Value of the number of bits used to encoded the given value, ie the codeword size
     */
    int encoder(unsigned short pred);
    
    /**
     * Insert padding into byte when no more values to encode (when file has ended)
     */
    void finalize();

protected:
    
    /**
     * Receives the quotient value and writes to the file a sequence of q 1's and a zero at end
     * @param q Quotient value, number of ones to be written
     */
    void decodeAndWriteValUnary(unsigned int q);

    /* Properties */
    /** Pointer to a obitstream in order to write bits to a file */
    obitstream *obs;

};

#endif	/* GOLOMBRICEENCODER_HPP */

