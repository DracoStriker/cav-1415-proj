/* 
 * File:   golombDecoder.hpp
 * Author: nuno
 *
 * Created on November 15, 2014, 4:16 PM
 */

#ifndef GOLOMBRICEDECODERAUDIO_HPP
#define	GOLOMBRICEDECODERAUDIO_HPP

#include "GolombRiceModuleAudio.hpp"

class GolombRiceDecoderAudio : public GolombRiceModuleAudio{
public:

    GolombRiceDecoderAudio(unsigned int m, istream *is);
    //golombDecoder(const golombDecoder& orig);
    virtual ~GolombRiceDecoderAudio();
    
    /**
     * Receives the M parameter and the pointer to the stream, reads the necessary bits and returns the value
     * in a Sign+Module way
     * @return Value of a byte in a Sign+Module way
     */
    unsigned int decoder();
    
    
protected:
    
    /* Properties */
    /** Pointer to ibitstream in order to read the bits of a file */
    ibitstream *ibs;

};

#endif	/* GOLOMBRICEDECODERAUDIO_HPP */

