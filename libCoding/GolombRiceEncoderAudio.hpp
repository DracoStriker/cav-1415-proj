/* 
 * File:   golombEncoder.hpp
 * Author: nuno
 *
 * Created on November 15, 2014, 4:15 PM
 */

#ifndef GOLOMBRICEENCODERAUDIO_HPP
#define	GOLOMBRICEENCODERAUDIO_HPP

#include "GolombRiceModuleAudio.hpp"

class GolombRiceEncoderAudio : public GolombRiceModuleAudio{
public:
    
    GolombRiceEncoderAudio(unsigned int m, ostream* os);
    //golombEncoder(const golombEncoder& orig);
    virtual ~GolombRiceEncoderAudio();
    
    /**
     * Encodes the value using GolombRice method and writes the contents of bitWrapper directly to file.
     * @param pred Value of the prediction obtained (Sign+Module)
     * @return Value of the number of bits used to encoded the given value, ie the codeword size
     */
    int encoder(unsigned int pred);
    
    /**
     * Insert padding into byte when no more values to encode (when file has ended)
     */
    void finalize();

protected:
    
    /**
     * Receives the quotient value and writes to the file a sequence of q 1's and a zero at end
     * @param q Quotient value, number of ones to be written
     */
    void decodeAndWriteValUnary(unsigned int q);

    /* Properties */
    /** Pointer to a obitstream in order to write bits to a file */
    obitstream *obs;

};

#endif	/* GOLOMBRICEENCODERAUDIO_HPP */

