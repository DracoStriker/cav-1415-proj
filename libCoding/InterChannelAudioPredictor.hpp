/* 
 * File:   InterChannelAudioPredictor.h
 * Author: simon
 *
 * Created on Dezember 13, 2014, 17:30 PM
 */

#ifndef INTER_CHANNEL_AUDIO_PREDICTOR_H
#define	INTER_CHANNEL_AUDIO_PREDICTOR_H

class InterChannelAudioPredictor {
public:
    InterChannelAudioPredictor();
    virtual ~InterChannelAudioPredictor();
    unsigned int encode(short, short);
    short decode(unsigned int, short);
    int getDelta();
    
private:
	int delta;
};

#endif	/* INTER_CHANNEL_AUDIO_PREDICTOR_H */
