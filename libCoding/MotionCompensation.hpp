/* 
 * File:   MotionCompensation.hpp
 * Author: simon
 *
 * Created on November 18, 2014, 6:07 PM
 */

#ifndef MOTIONCOMPENSATION_HPP
#define	MOTIONCOMPENSATION_HPP

#include "../libRGBYUV/Frame.hpp"
#include "../libRGBYUV/Block.hpp"

class MotionCompensation {
public:
	MotionCompensation();
    MotionCompensation(unsigned int);
    MotionCompensation(unsigned int, unsigned int);
    MotionCompensation(unsigned int, unsigned int, unsigned int);
    MotionCompensation(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int);
    virtual ~MotionCompensation();
    void encode(Frame *, Frame *, unsigned short *);
    void decode(Frame *, unsigned short *, Frame *);
private:
	void setSearchLimits(Frame &, Block &, unsigned int, unsigned int, unsigned int);
	unsigned int Delta;
	int minDeltaX;
	int minDeltaY;
	int maxDeltaX;
	int maxDeltaY;
	Block blk;
	Block prevBlk;
	bool lossy;
	int factorY;
	int factorU;
	int factorV;
};

#endif	/* MOTIONCOMPENSATION_HPP */

