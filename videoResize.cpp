/* 
 * File:   videoResize.cpp
 * Author: simon
 *
 * Created on October 2, 2014, 2:04 AM
 */

#include <cstdlib>
#include <cmath>
#include <getopt.h>
#include <unistd.h>
#include <sys/time.h>

#include "libRGBYUV/Video.hpp"
#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/VideoFromCamera.hpp"
#include "libRGBYUV/VideoFromFile.hpp"
#include "libRGBYUV/VideoToFile.hpp"

void resizeNearestNeighbor(Frame *, Frame *);
void resizeBilinear(Frame *, Frame *);
void printUsage();
void parseOpt(int, char**);
bool fexists(const string&);

string fileName, outName;
int dev, n;
bool isDev;
unsigned int scale = 2;
bool shrink = false;
bool optimized = false;

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    Video *video;
    VideoToFile *file;

    parseOpt(argc, argv);

    if (n != 1) {
        printUsage();
        return 0;
    }

    if (isDev) {
        video = new VideoFromCamera(dev);
    } else {
        if (!fexists(fileName)) {
            cerr << "Error opening file  " << fileName << " or file does not exist" << endl;
            return -1;
        }
        video = new VideoFromFile(fileName);
    }

    if (shrink) {
        file = new VideoToFile(outName, video->getNCols() / scale, video->getNRows() / scale, video->getFps(), video->getMode());
    } else {
        file = new VideoToFile(outName, video->getNCols() * scale, video->getNRows() * scale, video->getFps(), video->getMode());
    }

    Frame *in = video->genFrame();
    Frame *out = file->genFrame();

    void (*resize)(Frame *, Frame*);

    if (optimized) {
        resize = resizeBilinear;
    } else {
        resize = resizeNearestNeighbor;
    }

    int frame = 0;
    while (!video->readFrame(*in)) {

        cout << frame++ << endl;

        /* resize frame */
        resize(in, out);

        /* write resized frame*/
        file->writeFrame(*out);

        /* if camera press key to stop */
        if (isDev) usleep(1.0 / video->getFps() * 1000000);
    }

    return 0;
}

void resizeNearestNeighbor(Frame *frame, Frame *resized) {
    int w1 = frame->getNCols();
    int h1 = frame->getNRows();
    int w2 = resized->getNCols();
    int h2 = resized->getNRows();
    double x_ratio = w1 / (double) w2;
    double y_ratio = h1 / (double) h2;
    int px, py;
    for (int i = 0; i < h2; i++) {
        for (int j = 0; j < w2; j++) {
            px = (int) floor(j * x_ratio);
            py = (int) floor(i * y_ratio);
            unsigned char pixel1 = frame->getPixelValChannel(py, px, 1);
            unsigned char pixel2 = frame->getPixelValChannel(py, px, 2);
            unsigned char pixel3 = frame->getPixelValChannel(py, px, 3);
            resized->setPixelValChannel(i, j, pixel1, 1);
            resized->setPixelValChannel(i, j, pixel2, 2);
            resized->setPixelValChannel(i, j, pixel3, 3);
        }
    }
}

void resizeBilinear(Frame *frame, Frame *resized) {
    int w1 = frame->getNCols();
    int h1 = frame->getNRows();
    int w2 = resized->getNCols();
    int h2 = resized->getNRows();
    int x, y, index;
    float x_ratio = ((float) (w1)) / w2;
    float y_ratio = ((float) (h1)) / h2;
    float x_diff, y_diff, blue, red, green;
    for (int i = 0; i < h2; i++) {
        for (int j = 0; j < w2; j++) {
            x = (int) (x_ratio * j);
            y = (int) (y_ratio * i);
            x_diff = (x_ratio * j) - x;
            y_diff = (y_ratio * i) - y;
            index = (y * w2 + x);
            int Ar = frame->getPixelValChannel(y, x, 1);
            int Ag = frame->getPixelValChannel(y, x, 2);
            int Ab = frame->getPixelValChannel(y, x, 3);
            int Br = frame->getPixelValChannel(y, x + 1, 1);
            int Bg = frame->getPixelValChannel(y, x + 1, 2);
            int Bb = frame->getPixelValChannel(y, x + 1, 3);
            int Cr = frame->getPixelValChannel(y + 1, x, 1);
            int Cg = frame->getPixelValChannel(y + 1, x, 2);
            int Cb = frame->getPixelValChannel(y + 1, x, 3);
            int Dr = frame->getPixelValChannel(y + 1, x + 1, 1);
            int Dg = frame->getPixelValChannel(y + 1, x + 1, 2);
            int Db = frame->getPixelValChannel(y + 1, x + 1, 3);

            // blue element
            // Yb = Ab(1-w)(1-h) + Bb(w)(1-h) + Cb(h)(1-w) + Db(wh)
            blue = Ab * (1 - x_diff)*(1 - y_diff) + Bb * (x_diff)*(1 - y_diff) +
                    Cb * (y_diff)*(1 - x_diff) + Db * (x_diff * y_diff);

            // green element
            // Yg = Ag(1-w)(1-h) + Bg(w)(1-h) + Cg(h)(1-w) + Dg(wh)
            green = Ag * (1 - x_diff)*(1 - y_diff) + Bg * (x_diff)*(1 - y_diff) +
                    Cg * (y_diff)*(1 - x_diff) + Dg * (x_diff * y_diff);

            // red element
            // Yr = Ar(1-w)(1-h) + Br(w)(1-h) + Cr(h)(1-w) + Dr(wh)
            red = Ar * (1 - x_diff)*(1 - y_diff) + Br * (x_diff)*(1 - y_diff) +
                    Cr * (y_diff)*(1 - x_diff) + Dr * (x_diff * y_diff);

            resized->setPixelValChannel(i, j, (unsigned char) red, 1);
            resized->setPixelValChannel(i, j, (unsigned char) green, 2);
            resized->setPixelValChannel(i, j, (unsigned char) blue, 3);
        }
    }
}

void printUsage() {
    cout << "videoComp" << endl << "-c : camera device" << endl << "-f : video file" << endl << "-s : scale " << "-d : shrink" << "-o : rescaled file" << endl;
}

void parseOpt(int argc, char** argv) {
    int c;
    n = 0;
    while ((c = getopt(argc, argv, "c:f:s:w:doh")) != -1) {
        switch (c) {
            case 'c':
                isDev = true;
                dev = atoi(optarg);
                n++;
                break;
            case 'f':
                isDev = false;
                fileName = optarg;
                n++;
                break;
            case 's':
                scale = atoi(optarg);
                break;
            case 'd':
                shrink = true;
                break;
            case 'w':
                outName = optarg;
                break;
            case 'o':
                optimized = true;
                break;
            case 'h':
            default:
                printUsage();
        }
    }
}

/**
 * 
 * @param filename
 * @return 
 */
bool fexists(const string& filename) {
    ifstream ifile(filename.c_str());
    return ifile;
}
