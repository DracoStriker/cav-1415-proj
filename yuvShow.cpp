/* 
 * File:   yuvShow.cpp
 * Author: simon
 *
 * Created on October 2, 2014, 1:57 AM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <opencv2/opencv.hpp>
#include "libRGBYUV/Block.hpp"
#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/Video.hpp"
#include "libRGBYUV/VideoFromCamera.hpp"
#include "libRGBYUV/VideoFromFile.hpp"
 

using namespace std;
using namespace cv;

void printUsage();

int main(int argc, char** argv) {

    bool playing = true;
    char inputKey = '?';
    string title = "";
    Video * video;    

    /* Checking arguments */
    if(argc == 3){
        if(string(argv[1]) == "-h"){
            printUsage();
            return -1;
        }
        else if(string(argv[1]) == "-c"){
            video = new VideoFromCamera(atoi(argv[2]));
        }else if(string(argv[1]) == "-f"){
            
            /* Checking if file exists */
            ifstream myfile(argv[2]);
            if (!myfile.is_open()) {
                cerr << "Error opening file or file does not exist\n";
                return -1;
            }else{
                myfile.close();
            }
            video = new VideoFromFile(argv[2]);
        }
         
    }else{
        printUsage();
        return -1;
    }

    cout << "Press 'p' to Play/Pause\nPress 'q' to Quit" << endl;
    
    Frame * frame = video->genFrame();
    
    /* Preparing file title according to input */
    if(frame->getType() == RGB)
        title += "RGB "+string(argv[2]);
    else if(frame->getType() == YUV_420)
        title += "YUV420 "+string(argv[2]);
    else{
        cout << "Unknown file format\n";
        return -1;
    }
    /* Initialize OpenCv stuctures */
    cvNamedWindow(title.c_str(), CV_WINDOW_AUTOSIZE);
    
    /* Filling the matrix with frame data */
    Mat img(video->getNRows(), video->getNCols(), CV_8UC3);// frame->getDisplayBuffer());
    
    while(!video->readFrame(*frame)){
        
        /* Filling the matrix with frame data */
        //Mat img(video->getNRows(), video->getNCols(), CV_8UC3, frame->getDisplayBuffer());
        img.data = frame->getDisplayBuffer();
        /* display the image */
        imshow(title.c_str(), img);

        if (playing)
            inputKey = waitKey(1.0 / video->getFps() * 1000);   /* wait according to the frame rate */
        else
            inputKey = waitKey(0);      /* wait until the user press a key */

        /* process the pressed keys, if any */
        switch ((char) inputKey) {
            case 'q':
                return 0;
                
            case 'p':
                playing = playing ? false : true;
                break;
        }        
    }

    return 0;
}

void printUsage(){
    cout << "Usage: ./yuvShow <flag> <filename/device> "                  << endl <<
            "\tFlag:\n"                                                   <<
                    "\t\t-h, --help\tshow this help message and exit\n"   <<
                    "\t\t-c, --camera\tinput from camera\n"               <<
                    "\t\t-f, --file\tinput from stored file\n"            <<
            "\tFilename/Device:\n"                                        <<
                              "\t\tse flag -f\tnome do ficheiro\n"        <<
                              "\t\tse flag -c\tnúmero do dispositivo\n"   <<
            "\te.g: ./yuvShow -f cambada1.rgb yuv420\n"              <<
                   "\t     ./yuvShow -c 0 2"                         << endl;
}
