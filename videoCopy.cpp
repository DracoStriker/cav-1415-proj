/* 
 * File:   videoCopy.cpp
 * Author: simon
 *
 * Created on October 2, 2014, 2:02 AM
 */

#include <cstdlib>
#include "libRGBYUV/VideoFromFile.hpp"
#include "libRGBYUV/Block.hpp"
#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/VideoToFile.hpp"
#include "libRGBYUV/VideoFromCamera.hpp"
#include "libRGBYUV/Video.hpp"
#include <unistd.h>

using namespace std;

void printUsage();
/*
 * 
 */
int main(int argc, char** argv) {
    Video* video;
    bool camera = false;
    
    if (argc > 3 || argc < 2) {
        printUsage();
        return 0;
    } else {
        if (string(argv[1])== "-c") {
            video = new VideoFromCamera(atoi(argv[2]));
            camera=true;
        } else if (string(argv[1]) == "-f") {
            /* Checking if file exists */
            ifstream myfile(argv[2]);
            if (!myfile.is_open()) {
                cerr << "Error opening file or file does not exist\n";
                return -1;
            }else{
                myfile.close();
            }
            video = new VideoFromFile(argv[2]);
        } else{
            printUsage();
            return 0;
        }
		
    }
    
    
    Frame *inFrame = video->genFrame();
    Frame *outFrame = video->genFrame();
    string outFileName= inFrame->getType() == RGB ? "output.rgb" : "output.yuv";
    VideoToFile output(outFileName, *video);
    Block inBlock1(11, 11);
    Block inBlock2(11, 11);
    Block inBlock3(11, 11);
    Block outBlock1(11, 11);
    Block outBlock2(11, 11);
    Block outBlock3(11, 11);
    

    while (!video->readFrame(*inFrame)) {
        for (int i = 0; i < inFrame->getXBlockRows(inBlock1); i++) {
            for (int j = 0; j < inFrame->getXBlockCols(inBlock1); j++) {                
                inFrame->getExclusiveBlockVal(i, j, &inBlock1, 1);
                inBlock1.copyBlock(outBlock1);
                outFrame->setExclusiveBlockVal(outBlock1, i, j, 1);
            }
		}
           
        for (int i = 0; i < inFrame->getYBlockRows(inBlock2); i++) {
            for (int j = 0; j < inFrame->getYBlockCols(inBlock2); j++) {                
                inFrame->getExclusiveBlockVal(i, j, &inBlock2, 2);
                inBlock2.copyBlock(outBlock2);
                outFrame->setExclusiveBlockVal(outBlock2, i, j, 2);
            }
		}
		
		for (int i = 0; i < inFrame->getZBlockRows(inBlock3); i++) {
            for (int j = 0; j < inFrame->getZBlockCols(inBlock3); j++) {                
                inFrame->getExclusiveBlockVal(i, j, &inBlock3, 3);
                inBlock3.copyBlock(outBlock3);
                outFrame->setExclusiveBlockVal(outBlock3, i, j, 3);
            }
		}
		   
        output.writeFrame(*outFrame);
        
        /* if camera, delay the speed */
        if (camera) sleep(1.0 / video->getFps() * 1000);
        
    }
    return 0;
}

void printUsage() {
    cout << "Usage:"<<endl<< "./videoCopy <source flag> <source name>" << endl << endl << "source flag:" << endl << "-c -> camera device" << endl << "-f -> video file" << endl;
}

