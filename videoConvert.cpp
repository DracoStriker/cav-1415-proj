/* 
 * File:   videoConvert.cpp
 * Author: simon
 *
 * Created on October 2, 2014, 2:08 AM
 */

#include "termios.h"
#include "unistd.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <opencv2/opencv.hpp>
#include "libRGBYUV/Block.hpp"
#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/RGB_Frame.hpp"
#include "libRGBYUV/YUV420_Frame.hpp"
#include "libRGBYUV/Video.hpp"
#include "libRGBYUV/VideoFromCamera.hpp"
#include "libRGBYUV/VideoFromFile.hpp"
#include "libRGBYUV/VideoToFile.hpp"

using namespace std;

void printUsage();

/*
 * 
 */
int main(int argc, char** argv) {

    bool cameraRec = false;
    string name, outputFormat;
    Video * video;
    VideoFormat convertTo;    
    
    /* Checking arguments */
    if( argc == 4 ){
        if(string(argv[1]) == "-h"){
            printUsage();
            return -1;
        }
        else if(string(argv[1]) == "-c"){
            video = new VideoFromCamera(atoi(argv[2]));
            name = "camera";
            cameraRec = true;
        }else if(string(argv[1]) == "-f"){
            
            /* Checking if file exists */
            ifstream myfile(argv[2]);
            if (!myfile.is_open()) {
                cerr << "Error opening file or file does not exist\n";
                return -1;
            }else{
                myfile.close();
            }
            
            video = new VideoFromFile(argv[2]);
            name = argv[2];
        }
        outputFormat = argv[3];
    }else{
        /*cout << "If video from Camera ('Q' to stop recording & converting)" << endl <<*/
        cout << "Invalid input arguments\n";
        printUsage();
        return -1;
    }
    
    /* to lower case output format*/
    transform(outputFormat.begin(), outputFormat.end(), outputFormat.begin(), ::tolower);
    
    /* Decode output format. 1-RGB, 2-YUV_420 (not case sensitive) */
    if( (atoi(outputFormat.c_str()) == 1) || !outputFormat.compare("rgb") ){
        convertTo = RGB;        // Format to be converted
        name+=".rgb";           // Adding extension to the name
    }else if( (atoi(outputFormat.c_str()) == 2 ) || !outputFormat.compare("yuv420") ){
        convertTo = YUV_420;    // Format to be converted
        name+=".yuv";           // Adding extension to the name
    }else {
        cout << "Unknown format '" << outputFormat << "'" << endl;
        printUsage();
        return -1;
    }
    
    /* Verify if input and output formats are equal */
    if(video->getMode() == convertTo){
        cout << "The video you pretend to convert is already in the specified format." << endl <<
                "Formats available: 1-RGB, 2-YUV420" << endl;
        return -1;              
    }     
    
    /* Generating frames */
    Frame * frame = video->genFrame();
    Frame * newFrame = video->genConvToFrame(); /* Creating frame of format to be converted to */

    /* Writing the header information of new file */
    VideoToFile *videoToFile = new VideoToFile(name, video->getNCols(), video->getNRows(), video->getFps(), convertTo);
    
    cout << "Starting converting video to " << outputFormat << endl;

    while(!video->readFrame(*frame)){
                 
        /* Converting frame */
        if( frame->convert(newFrame, convertTo) != 0){
            cout << "Unknown video format!\n";
            return -1;      //error ocurred
        }
        
        /* Writing frame to file */
        videoToFile->writeFrame( *newFrame );
                                
    }
    
    cout << "Video Converted successfully!" << endl;
    
    return 0;
}

void printUsage(){
    cout << "Usage: ./videoConvert <flag> <filename/device> <outputFormat>"    << endl <<
            "\tFlag:\n"                                                   <<
                    "\t\t-h, --help\tshow this help message and exit\n"   <<
                    "\t\t-c, --camera\tinput from camera\n"               <<
                    "\t\t-f, --file\tinput from stored file\n"            <<
            "\tFilename/Device:\n"                                        <<
                              "\t\tse flag -f\tnome do ficheiro\n"        <<
                              "\t\tse flag -c\tnúmero do dispositivo\n"   <<
            "\tOutputFormat (not case sensitive):\n"                      <<
                "\t\t'rgb' or 1\tconvert to RGB format\n"                 <<
                "\t\t'yuv420' or 2\tconvert to YUV420 format\n"           << endl <<
            "\te.g: ./videoConvert -f cambada1.rgb yuv420\n"                   <<
                   "\t     ./videoConvert -c 0 2"                              << endl;
}
