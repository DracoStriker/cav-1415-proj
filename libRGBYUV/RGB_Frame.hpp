/* 
 * File:   RGB_Frame.hpp
 * Author: nuno
 *
 * Created on 7 de Outubro de 2014, 20:03
 */

#ifndef RGB_FRAME_HPP
#define	RGB_FRAME_HPP

#include "Frame.hpp"

using namespace std;
using namespace cv;

class RGB_Frame : public Frame{
public:
    RGB_Frame(unsigned int nRows, unsigned int nCols);
    ~RGB_Frame();
    
    void setData(unsigned char* src);
    void displayFrame(int time);
    int getPixelVal(int row, int col, uchar val[]);
    unsigned char getPixelValChannel(int row, int col, int ch);
    int setPixelVal(int row, int col, uchar val[]);
    int setPixelValChannel(int row, int col, uchar val, int ch);
    int getBlockVal(int row, int col, Block* blk, int ch);
    int getExclusiveBlockVal(int row, int col, Block* blk, int ch);
    int checkExclusiveBlockPos(int row, int col, Block* blk, int ch);
    int setBlockVal(Block &blk, int row, int col, int ch);
    int setExclusiveBlockVal(Block &blk, int row, int col, int ch);
    unsigned char* ptr();
    unsigned char* getDisplayBuffer();
    
protected:
    
    /** Inner method that groups the 3 differents channels into one buffer in packed mode 
     *
     *  @return Buffer data of the frame, in packed mode
     */
    unsigned char *joinChannels();
    unsigned char *joinBGRChannels();
};


#endif	/* RGB_FRAME_HPP */

