/* 
 * File:   VideoFromFile.cpp
 * Author: simon
 * 
 * Created on October 7, 2014, 7:54 PM
 */

#include <sstream>
#include <vector>

#include "VideoFromFile.hpp"

VideoFromFile::VideoFromFile(string fileName) : file(fileName), fileName(fileName) {
    string line, mode;
    getline(file, line);
    istringstream(line) >> nCols >> nRows >> fps >> mode;
    this->mode = str2VideoFormat(mode);
}

VideoFromFile::~VideoFromFile() {
}

int VideoFromFile::readFrame(Frame &frame) {
    if (file.eof()) return -1;
    vector<unsigned char> buffer(frame.getFrameSize(), 0);
    if (!file.read((char *)buffer.data(), buffer.size())) return -1;
    frame.setData(buffer.data());
    return 0;
}

ostream& operator<<(ostream &s, const VideoFromFile &v) {
    return s << "Video(Height = " << v.nRows << ", Width = " << v.nCols << ", FPS = " << v.fps << ", Mode = " << v.mode << ", File = " << v.fileName << ")";
}

VideoFormat VideoFromFile::str2VideoFormat(string &s) {
    if (s == "rgb") return VideoFormat::RGB;
    else if (s == "420") return VideoFormat::YUV_420;
}