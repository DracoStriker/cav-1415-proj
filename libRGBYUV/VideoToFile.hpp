/* 
 * File:   VideoToFile.hpp
 * Author: simon
 *
 * Created on October 15, 2014, 12:33 PM
 */

#ifndef VIDEOTOFILE_HPP
#define	VIDEOTOFILE_HPP

#include <string>
#include <fstream>

#include "Video.hpp"
#include "Frame.hpp"

class VideoToFile {
public:
    VideoToFile(string, unsigned int, unsigned int, unsigned int, VideoFormat);
    VideoToFile(string, Video &);
    ~VideoToFile();
    int writeFrame(Frame &);
    unsigned int getNCols();
    unsigned int getNRows();
    unsigned int getFps();
    VideoFormat getMode();
    Frame *genFrame();
    friend ostream& operator<<(ostream &, const VideoToFile &);
protected:
    unsigned int nRows, nCols, fps;
    VideoFormat mode;
    ofstream file;
private:
    string videoFormat2Str(VideoFormat);
    string fileName;
};

#endif	/* VIDEOTOFILE_HPP */

