/* 
 * File:   VideoToFile.cpp
 * Author: simon
 * 
 * Created on October 15, 2014, 12:33 PM
 */

#include "VideoToFile.hpp"
#include "RGB_Frame.hpp"
#include "YUV420_Frame.hpp"

VideoToFile::VideoToFile(string fileName, unsigned int nCols, unsigned int nRows, unsigned int fps, VideoFormat mode) : file(fileName), fileName(fileName), nCols(nCols), nRows(nRows), fps(fps), mode(mode) {
    file << this->nCols << " " << this->nRows << " " << this->fps << " " << videoFormat2Str(this->mode) << endl;
}

VideoToFile::VideoToFile(string fileName, Video &video) : file(fileName), fileName(fileName), nCols(video.getNCols()), nRows(video.getNRows()), fps(video.getFps()), mode(video.getMode()) {
    file << nCols << " " << nRows << " " << fps << " " << videoFormat2Str(mode) << endl;
}

VideoToFile::~VideoToFile() {
}

int VideoToFile::writeFrame(Frame &frame) {
    file.write((const char *) frame.ptr(), frame.getFrameSize());
}

unsigned int VideoToFile::getNCols() {
    return nCols;
}

unsigned int VideoToFile::getNRows() {
    return nRows;
}

unsigned int VideoToFile::getFps() {
    return fps;
}

VideoFormat VideoToFile::getMode() {
    return mode;
}

Frame *VideoToFile::genFrame() {
    switch (mode) {
        case RGB:
            return new RGB_Frame(nRows, nCols);
            break;
        case YUV_420:
            return new YUV420_Frame(nRows, nCols);
            break;
        default:
            return nullptr;
    }
}

ostream& operator<<(ostream &s, const VideoToFile &v) {
    return s << "Video(Height = " << v.nRows << ", Width = " << v.nCols << ", FPS = " << v.fps << ", Mode = " << v.mode << ", File = " << v.fileName << ")";
}

string VideoToFile::videoFormat2Str(VideoFormat mode) {
    switch (mode) {
        case RGB:
            return "rgb";
            break;
        case YUV_420:
            return "420";
            break;
    }
}