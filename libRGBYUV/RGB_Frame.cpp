/* 
 * File:   RGB_Frame.cpp
 * Author: nuno
 * 
 * Created on 29 de Setembro de 2014, 16:22
 */

#include "RGB_Frame.hpp"

using namespace std;
using namespace cv;


RGB_Frame::RGB_Frame(unsigned int nRows, unsigned int nCols) {
    this->nRows = nRows;
    this->nCols = nCols;
    type = RGB;
    
    /* Calculating frame size */
    frameSize = nCols * nRows * cn;
    xRows = yRows = zRows = nRows;
    xCols = yCols = zCols = nCols;
    
    /* Initializing channel buffers */
    channel1 = new uchar[nRows*nCols];
    channel2 = new uchar[nRows*nCols];
    channel3 = new uchar[nRows*nCols];
    
    
    //cout << "RGB Frame created with " << nRows << " rows and " << nCols << " columns.\n";
}

RGB_Frame::~RGB_Frame() {
    delete[] channel3;
    delete[] channel2;
    delete[] channel1;
}


void RGB_Frame::setData(unsigned char* src) {
    
    /* auxiliar variable that indicates pos in channel buffer */
    uint pos = 0;

    /* Converting packed mode data to 3 Component */
    for (uint i = 0; i < nRows; i++)
        for (uint j = 0; j < nCols; j++) {
            channel1[pos] = src[i * nCols * cn + j * cn + 0]; // R 
            channel2[pos] = src[i * nCols * cn + j * cn + 1]; // G 
            channel3[pos] = src[i * nCols * cn + j * cn + 2]; // B 
            
            pos++;
        }

}

unsigned char* RGB_Frame::ptr(){
    return joinChannels();
}

unsigned char* RGB_Frame::getDisplayBuffer(){
    return joinBGRChannels();
}

unsigned char * RGB_Frame::joinChannels(){
    unsigned char *buffer = new uchar[nRows*nCols*cn]; 
    
    /* Join channel modifications into temporary buffer */
    for(uint i = 0; i < nRows*nCols; i++){
        buffer[i*cn] = channel1[i];
        buffer[i*cn+1] = channel2[i];
        buffer[i*cn+2] = channel3[i];
    }
    return buffer;
    
}

unsigned char * RGB_Frame::joinBGRChannels(){
    unsigned char *buffer = new uchar[nRows*nCols*cn]; 
    
    /* Join channel modifications into temporary buffer */
    for(uint i = 0; i < nRows*nCols; i++){
        buffer[i*cn] = channel3[i];
        buffer[i*cn+1] = channel2[i];
        buffer[i*cn+2] = channel1[i];
    }
    return buffer;
    
}

void RGB_Frame::displayFrame(int time) {
    
    /* Matrix with all components */
    Mat display(nRows, nCols, CV_8UC3);
    
    //cout << "nRows= " << nRows << " nCols= " << nCols <<"\n";
    
    /* Copy buffer data(frame) to the matrix data */
    memcpy(display.data, joinChannels(), nRows*nCols*cn);

    // Create a window for display.
    namedWindow("Display frame", WINDOW_AUTOSIZE);
    
    // Show frame
    imshow("Display frame", display);           
    waitKey(time);
    
}

int RGB_Frame::getPixelVal(int row, int col, uchar val[]) {

    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    //cout << "row = " << row << "col= " << col << "\n";
    val[0] = channel1[row*nCols + col];       //R
    val[1] = channel2[row*nCols + col];       //G
    val[2] = channel3[row*nCols + col];       //B
    
    return 0;
}

unsigned char RGB_Frame::getPixelValChannel(int row, int col, int ch) {

    //cout << "row = " << row << "col= " << col << "\n";
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    if(ch == 1)
        return channel1[row*nCols + col];       //R
    else if(ch == 2)
        return channel2[row*nCols + col];       //G
    else if(ch == 3)
        return channel3[row*nCols + col];       //B
    else{
        cout << "Invalid channel number!\n";
        throw ICE;
    }
    return 0;
}

int RGB_Frame::setPixelVal(int row, int col, uchar val[]){
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    channel1[row*nCols + col] = clamp(val[0]);
    channel2[row*nCols + col] = clamp(val[1]);
    channel3[row*nCols + col] = clamp(val[2]);   
    
    return 0;
}

int RGB_Frame::setPixelValChannel(int row, int col, uchar val, int ch){
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    if(ch == 1)
        channel1[row*nCols + col] = val;
    else if(ch == 2)
        channel2[row*nCols + col] = val;
    else if(ch == 3)
        channel3[row*nCols + col] = val;
    else{
        cout << "Invalid channel number!\n";
        return ICE;
    }
        
    return 0;
}

int RGB_Frame::getBlockVal(int row, int col, Block* blk, int ch){
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    /* Checking if looking for a valid channel*/
    if(ch <= 0 || ch > 3){
        cout << "Invalid channel number!\n";
        return ICE;
    }
    /*subBlock arrangement*/
    if(nRows-row < blk->getMaxRows())
        blk->setRows(nRows-row);
    else
        blk->setRows(blk->getMaxRows());
    if(nCols - col < blk->getMaxCols())
        blk->setCols(nCols-col);
    else
        blk->setCols(blk->getMaxCols());
    
    for(int i = 0; i < blk->getRows(); i++)
        for(int j = 0; j < blk->getCols(); j++)
            blk->setValue( i, j, getPixelValChannel(row, col, ch));

    blk->setXPos(col);
    blk->setYPos(row);
 
    return 0;
}

int RGB_Frame::getExclusiveBlockVal(int row, int col, Block* blk, int ch){

	if(ch <= 0 || ch > 3){
        cout << "Invalid channel number!\n";
        return ICE;
    }
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > getXBlockRows(*blk) || col < 0 || col > getXBlockCols(*blk)){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }

    int prow = row*blk->getMaxRows();
    int pcol = col*blk->getMaxCols();
    
    /*subBlock arrangement*/
    if(nRows-prow < blk->getMaxRows())
        blk->setRows(nRows-prow);
    else
        blk->setRows(blk->getMaxRows());
    if(nCols - pcol < blk->getMaxCols())
        blk->setCols(nCols-pcol);
    else
        blk->setCols(blk->getMaxCols());
        
    /* Go through the block and set the values */
    for(int i = 0; i < blk->getRows(); i++){
        for(int j = 0; j < blk->getCols(); j++){
            if(ch == 1)
                blk->setValue(i,j,channel1[nCols*(prow+i) + pcol+j]);  // Y
            else if(ch == 2)
                blk->setValue(i,j,channel2[nCols*(prow+i) + pcol+j]);  // U
            else
                blk->setValue(i,j,channel3[nCols*(prow+i) + pcol+j]);  // V 
		}
	}

    blk->setXPos(pcol);
    blk->setYPos(prow);
    
	return 0;
}

int RGB_Frame::checkExclusiveBlockPos(int row, int col, Block* blk, int ch){

    if(ch <= 0 || ch > 3){
        cout << "Invalid channel number!\n";
        return ICE;
    }
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > getXBlockRows(*blk) || col < 0 || col > getXBlockCols(*blk)){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }

    int prow = row*blk->getMaxRows();
    int pcol = col*blk->getMaxCols();
    
    /*subBlock arrangement*/
    if(nRows-prow < blk->getMaxRows())
        blk->setRows(nRows-prow);
    else
        blk->setRows(blk->getMaxRows());
    if(nCols - pcol < blk->getMaxCols())
        blk->setCols(nCols-pcol);
    else
        blk->setCols(blk->getMaxCols());

    blk->setXPos(pcol);
    blk->setYPos(prow);
    
    return 0;
}

int RGB_Frame::setBlockVal(Block &blk, int row, int col, int ch){
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    /*subBlock arrangement*/
    if(nRows-row < blk.getMaxRows())
        blk.setRows(nRows-row);
    else
        blk.setRows(blk.getMaxRows());
    if(nCols - col < blk.getMaxCols())
        blk.setCols(nCols-col);
    else
        blk.setCols(blk.getMaxCols());
    
    for(int i = 0; i < blk.getRows(); i++)
        for(int j = 0; j < blk.getCols(); j++){
            if(ch == 1)
                channel1[row*nCols + col] = blk.getValue(i,j);
            else if(ch == 2)
                channel2[row*nCols + col] = blk.getValue(i,j);
            else if(ch == 3)
                channel3[row*nCols + col] = blk.getValue(i,j);
            else{
                cout << "Invalid channel number!\n";
                return ICE;
            }
        }
    
    return 0;
}

int RGB_Frame::setExclusiveBlockVal(Block &blk, int row, int col, int ch){
	
	if(ch <= 0 || ch > 3){
        cout << "Invalid channel number!\n";
        return ICE;
    }
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > getXBlockRows(blk) || col < 0 || col > getXBlockCols(blk)){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }

    int prow = row*blk.getMaxRows();
    int pcol = col*blk.getMaxCols();
    
    /*subBlock arrangement*/
    if(nRows-prow < blk.getMaxRows())
        blk.setRows(nRows-prow);
    else
        blk.setRows(blk.getMaxRows());
    if(nCols - pcol < blk.getMaxCols())
        blk.setCols(nCols-pcol);
    else
        blk.setCols(blk.getMaxCols());
    
    /* Go through the block and set the values */
    for(int i = 0; i < blk.getRows(); i++)
        for(int j = 0; j < blk.getCols(); j++){
            if(ch == 1)
                channel1[nCols*(prow+i) + pcol+j] = blk.getValue(i,j);  // Y
            else if(ch == 2)
                channel2[nCols*(prow+i) + pcol+j] = blk.getValue(i,j);  // U
            else
                channel3[nCols*(prow+i) + pcol+j] = blk.getValue(i,j);  // V           
        }

	return 0;
}
