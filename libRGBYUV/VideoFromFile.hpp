/* 
 * File:   VideoFromFile.hpp
 * Author: simon
 *
 * Created on October 7, 2014, 7:54 PM
 */

#ifndef VIDEOFROMFILE_HPP
#define	VIDEOFROMFILE_HPP

#include <fstream>

#include "Frame.hpp"
#include "Video.hpp"

using namespace std;

class VideoFromFile : public Video {
public:
    VideoFromFile(string);
    ~VideoFromFile();
    int readFrame(Frame &);
    friend ostream& operator<<(ostream &, const VideoFromFile &);
protected:
    ifstream file;
private:
    VideoFormat str2VideoFormat(string &s);
    string fileName;
};

#endif	/* VIDEOFROMFILE_HPP */

