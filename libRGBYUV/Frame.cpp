/* 
 * File:   Frame.cpp
 * Author: nuno
 * 
 * Created on 29 de Setembro de 2014, 13:32
 */

#include "Frame.hpp"

using namespace std;
using namespace cv;

/* Destructor */
Frame::~Frame() {
}

void Frame::getChannels(unsigned char** c1, unsigned char** c2, unsigned char** c3) {
    *c1 = channel1;
    *c2 = channel2;
    *c3 = channel3;
}

int Frame::convert(Frame* newFrame, VideoFormat newFormat) {

    /*  Auxiliar variable */
    uint pos = 0;
    
    /* If input format is the same of output format return a copy*/
    if (type == newFormat) {
        //cout << "Same input and output format. Returning a copy.\n";
        newFrame->setData(ptr());
    }        
    /* Convert to RGB */
    else if (newFormat == RGB) {//&& type != newFormat){

        /* Initializing new frame values */
        newFrame->nRows = xRows;
        newFrame->nCols = xCols;


        /* Converting */
        for (uint i = 0; i < nRows; i++)
            for (uint j = 0; j < nCols; j++) {
                /* R = 1.164(Y - 16) + 1.596(U - 128) */
                newFrame->channel1[pos] = clamp(1.164 * (channel1[i * nCols + j] - 16) +
                        1.596 * (channel3[(i / 2)*(nCols / 2) + (j / 2)] - 128));

                /* G = 1.164(Y - 16) - 0.813(V - 128) - 0.391(U - 128) */
                newFrame->channel2[pos] = clamp(1.164 * (channel1[i * nCols + j] - 16)
                        - 0.813 * (channel2[(i / 2)*(nCols / 2) + (j / 2)] - 128)
                        - 0.391 * (channel3[(i / 2)*(nCols / 2) + (j / 2)] - 128));
                /* B = 1.164(Y - 16) + 2.018(V - 128) */
                newFrame->channel3[pos] = clamp(1.164 * (channel1[i * nCols + j] - 16)
                        + 2.018 * (channel2[(i / 2)*(nCols / 2) + (j / 2)] - 128));
                //cout << "Row= " << i << " Col= " << j << " pos= " << pos << " val Y= "<< (int)newFrame->channel1[pos] << "\n";

                pos++;
            }
        //cout << "nRows= " << nRows << " nCols= " << nCols << "\n";
        newFrame->type = RGB;

    }/* Convert to YUV_420*/
    else if (newFormat == YUV_420) {//&& type != newFormat) {

        /* Initializing new frame values */
        newFrame->xCols = nCols;
        newFrame->xRows = nRows;
        newFrame->yCols = newFrame->zCols = nCols / 2;
        newFrame->zRows = newFrame->yRows = nRows / 2;
        //uint total = xRows*xCols + yRows*yCols/2;

        for (uint i = 0; i < nRows; i++)
            for (uint j = 0; j < nCols; j++) {
                /* Y  =      (0.257 * R) + (0.504 * G) + (0.098 * B) + 16 */
                newFrame->channel1[i * nCols + j] = 0.257 * channel1[pos] + 0.504 * channel2[pos] + 0.098 * channel3[pos] + 16;
                /* Cb = U = -(0.148 * R) - (0.291 * G) + (0.439 * B) + 128 */
                newFrame->channel2[(i / 2)*(nCols / 2) + (j / 2)] = -0.148 * channel1[pos] - 0.291 * channel2[pos]
                        + 0.439 * channel3[pos] + 128;
                /* Cr = V =  (0.439 * R) - (0.368 * G) - (0.071 * B) + 128 */
                newFrame->channel3[(i / 2)*(nCols / 2) + (j / 2)] = 0.439 * channel1[pos] - 0.368 * channel2[pos]
                        - 0.071 * channel3[j] + 128;
                //cout << "Row= " << i << "Col= " << j << "pos= " << pos << "val Y= "<< (int)newFrame->channel1[i*nCols + j] << "\n";

                pos++;
            }
        //cout << "nRows= " << nRows << " nCols= " << nCols << "\n";

        newFrame->type = YUV_420;

    }/* Unknown video format or asking to convert to same format */
    else {
        return IOB;
        //cout << "Unknown video format!\n";
    }

    return 0;
}

int Frame::getFrameSize() {
    return frameSize;
}

VideoFormat Frame::getType(){
    return type;
}


int Frame::getNRows() {
    return nRows;
}

int Frame::getXRows() {
    return xRows;
}

int Frame::getYRows() {
    return yRows;
}

int Frame::getZRows() {
    return zRows;
}

int Frame::getNCols() {
    return nCols;
}

int Frame::getXCols() {
    return xCols;
}

int Frame::getYCols() {
    return yCols;
}

int Frame::getZCols() {
    return zCols;
}

uchar Frame::clamp(int val) {

    if (val > 255) return 255;
    if (val < 0) return 0;

    return val;
}

int Frame::getXBlockRows(Block &blk){
	return (xRows+blk.getMaxRows()-1)/blk.getMaxRows();
}

int Frame::getYBlockRows(Block &blk){
	return (yRows+blk.getMaxRows()-1)/blk.getMaxRows();
}

int Frame::getZBlockRows(Block &blk){
	return (zRows+blk.getMaxRows()-1)/blk.getMaxRows();
}

int Frame::getXBlockCols(Block &blk){
	return (xCols+blk.getMaxCols()-1)/blk.getMaxCols();
}

int Frame::getYBlockCols(Block &blk){
	return (yCols+blk.getMaxCols()-1)/blk.getMaxCols();
}

int Frame::getZBlockCols(Block &blk){
	return (zCols+blk.getMaxCols()-1)/blk.getMaxCols();
}

