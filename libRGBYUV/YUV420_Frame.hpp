/* 
 * File:   YUV420_Frame.hpp
 * Author: nuno
 *
 * Created on 7 de Outubro de 2014, 20:05
 */

#ifndef YUV420_FRAME_HPP
#define	YUV420_FRAME_HPP

#include "Frame.hpp"
#include "RGB_Frame.hpp"

using namespace std;
using namespace cv;

class YUV420_Frame : public Frame{
public:
    YUV420_Frame(unsigned int nRows, unsigned int nCols);
    //YUV420_Frame(const YUV420_Frame& orig);
    virtual ~YUV420_Frame();
    
    void setData(unsigned char* src);
    void displayFrame(int time);
    
    int getPixelVal(int row, int col, uchar val[]);  
    unsigned char getPixelValChannel(int row, int col, int ch);
    int setPixelVal(int row, int col, uchar val[]);
    int setPixelValChannel(int row, int col, uchar val, int ch);
    int getBlockVal(int row, int col, Block* blk, int ch);
    int getExclusiveBlockVal(int row, int col, Block* blk, int ch);
    int checkExclusiveBlockPos(int row, int col, Block* blk, int ch);
    int setBlockVal(Block &blk, int row, int col, int ch);
    int setExclusiveBlockVal(Block &blk, int row, int col, int ch);
    unsigned char* ptr();
    unsigned char* getDisplayBuffer();

    
protected:
    RGB_Frame* rgb_frame;
};

#endif	/* YUV420_FRAME_HPP */

