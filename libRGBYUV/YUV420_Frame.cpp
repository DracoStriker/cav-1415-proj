                                                                                                                                                                                                                /* 
 * File:   YUV420_Frame.cpp
 * Author: nuno
 * 
 * Created on 6 de Outubro de 2014, 15:38
 */

#include "YUV420_Frame.hpp"

using namespace std;
using namespace cv;

YUV420_Frame::YUV420_Frame(unsigned int nRows, unsigned int nCols) {
    
    /* Initialize data structure */
    this->nRows = nRows;
    this->nCols = nCols;
    this->xRows = nRows;    this->xCols = nCols;
    this->yRows = this->zRows = xRows/2;  this->yCols = this->zCols = xCols/2;
    type = YUV_420;
    
    /* Calculating frame size */
    frameSize = nCols * nRows * 3/2;
    
    /* Initialize data buffer */
    buffer = new uchar[frameSize]; 
    rgb_frame = new RGB_Frame(nRows, nCols);
    
    /* Auxiliary variables */
    int pos_u = xRows*xCols;                // points to where U component starts  
    int pos_v = xRows*xCols + yRows*yCols;  // points to where V component starts
    
    /* Pointing channel buffers to data */
    channel1 = buffer;
    channel2 = &(buffer[pos_u]);
    channel3 = &(buffer[pos_v]);
    
    //cout << "YUV420 Frame created with " << nRows << " rows and " << nCols << " columns.\n";
    
    
}

YUV420_Frame::~YUV420_Frame() {
    delete rgb_frame;
    delete[] buffer;
}

unsigned char* YUV420_Frame::ptr(){
 
    return buffer;
}

unsigned char* YUV420_Frame::getDisplayBuffer(){
    
    /* Create new RGB Frame to be displayed after conversion*/
    //Frame * fr = new RGB_Frame(nRows, nCols);
    
    /* Convert to RGB */
    Frame::convert(rgb_frame, RGB);
    
    return rgb_frame->getDisplayBuffer();
}

void YUV420_Frame::setData(unsigned char* src) {
    
    /* Copy source data to internal buffer data */
    memcpy(buffer, src, nRows*nCols*3/2);
        
}

void YUV420_Frame::displayFrame(int time) {
    
    /* Create new RGB Frame to be displayed after conversion*/
    Frame * fr = new RGB_Frame(nRows, nCols);
    
    /* Convert to RGB */
    Frame::convert(fr, RGB);
    
    /* Matrix with all components */
    Mat display(nRows, nCols, CV_8UC3);
    
    /* Copy buffer data(frame) to the matrix data */
    memcpy(display.data, fr->ptr(), nRows*nCols*cn);
    
    /* Create a window for display. */
    namedWindow("Display frame", WINDOW_AUTOSIZE);
    
    /* Show frame */
    imshow("Display frame", display);           
    waitKey(time);
    
}

int YUV420_Frame::getPixelVal(int row, int col, uchar val[]) {

    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    val[0] = channel1[row*nCols + col];                   //Y
    val[1] = channel2[(row/2)*(nCols/2) + (col/2)];       //U
    val[2] = channel3[(row/2)*(nCols/2) + (col/2)];       //V
    
    return 0;
}

int YUV420_Frame::setPixelVal(int row, int col, uchar val[]){
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    channel1[row*nCols + col] = clamp(val[0]);
    channel2[(row/2)*(nCols/2) + (col/2)] = clamp(val[1]);
    channel3[(row/2)*(nCols/2) + (col/2)] = clamp(val[2]);
   
    return 0;
}


unsigned char YUV420_Frame::getPixelValChannel(int row, int col, int ch) {
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    if(ch == 1)
        return channel1[row*nCols + col];       // Y
    else if(ch == 2)
        return channel2[(row/2)*(nCols/2) + (col/2)];   // U
    else if(ch == 3)
        return channel3[(row/2)*(nCols/2) + (col/2)];   // V
    else{
        cout << "Invalid channel number!\n";
        throw ICE;
    }
    
    return 0;
    
}
    
int YUV420_Frame::setPixelValChannel(int row, int col, uchar val, int ch) {
    
    /* Checking if pixel position is between bounds of frame */
    if(row < 0 || row > nRows || col < 0 || col > nCols){
        cout << "Invalid Position Coordinates!\n";
        return IOB;
    }
    
    if(ch == 1)
        channel1[row*nCols + col] = clamp(val);
    else if(ch == 2)
        channel2[(row/2)*(nCols/2) + (col/2)] = clamp(val);
    else if(ch == 3)
        channel3[(row/2)*(nCols/2) + (col/2)] = clamp(val);
    else{
        cout << "Invalid channel number!\n";
        return ICE;
    }
        
}

int YUV420_Frame::getBlockVal(int row, int col, Block* blk, int ch){   
    int totalRows, totalCols;
    
    if(ch==1){
		totalRows=xRows;
		totalCols=xCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getXRows() || col < 0 || col >= getXCols()){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==2){
		totalRows=yRows;
		totalCols=yCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getYRows() || col < 0 || col >= getYCols()){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==3){
		totalRows=zRows;
		totalCols=zCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getZRows() || col < 0 || col >= getZCols()){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else{
        cout << "Invalid channel number!\n";
        return ICE;
    }
    
    /*subBlock arrangement*/
    if(totalRows-row < blk->getMaxRows())
        blk->setRows(totalRows-row);
    else
        blk->setRows(blk->getMaxRows());
    if(totalCols - col < blk->getMaxCols())
        blk->setCols(totalCols-col);
    else
        blk->setCols(blk->getMaxCols());
    
    /* Go through the block and set the values */
    for(int i = 0; i < blk->getRows(); i++){
        for(int j = 0; j < blk->getCols(); j++){
			if(ch==1)
            blk->setValue( i, j, channel1[totalCols*(row+i)+col+j]);
            else if(ch==2)
            blk->setValue( i, j, channel2[totalCols*(row+i)+col+j]);
            else
            blk->setValue( i, j, channel3[totalCols*(row+i)+col+j]);
		}
	}

    blk->setXPos(col);
    blk->setYPos(row);
        
    return 0;
}

int YUV420_Frame::getExclusiveBlockVal(int row, int col, Block* blk, int ch){
	
	int totalRows, totalCols;
    
    if(ch==1){
		totalRows=xRows;
		totalCols=xCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getXBlockRows(*blk) || col < 0 || col >= getXBlockCols(*blk)){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==2){
		totalRows=yRows;
		totalCols=yCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getYBlockRows(*blk) || col < 0 || col >= getYBlockCols(*blk)){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==3){
		totalRows=zRows;
		totalCols=zCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getZBlockRows(*blk) || col < 0 || col >= getZBlockCols(*blk)){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else{
        cout << "Invalid channel number!\n";
        return ICE;
    }

    int prow = row*blk->getMaxRows();
    int pcol = col*blk->getMaxCols();
    
    /*subBlock arrangement*/
    if(totalRows-prow < blk->getMaxRows())
        blk->setRows(totalRows-prow);
    else
		blk->setRows(blk->getMaxRows());
    if(totalCols - pcol < blk->getMaxCols())
        blk->setCols(totalCols-pcol);
    else
		blk->setCols(blk->getMaxCols());
       
        
    /* Go through the block and set the values */
    for(int i = 0; i < blk->getRows(); i++){
        for(int j = 0; j < blk->getCols(); j++){
            if(ch == 1)
                blk->setValue(i,j,channel1[totalCols*(prow+i) + pcol+j]);  // Y
            else if(ch == 2)
                blk->setValue(i,j,channel2[totalCols*(prow+i) + pcol+j]);  // U
            else
                blk->setValue(i,j,channel3[totalCols*(prow+i) + pcol+j]);  // V 
		}
	}

    blk->setXPos(pcol);
    blk->setYPos(prow);
        
    return 0;
}

int YUV420_Frame::checkExclusiveBlockPos(int row, int col, Block* blk, int ch){
    
    int totalRows, totalCols;
    
    if(ch==1){
        totalRows=xRows;
        totalCols=xCols;
        /* Checking if pixel position is between bounds of frame */
        if(row < 0 || row >= getXBlockRows(*blk) || col < 0 || col >= getXBlockCols(*blk)){
            cout << "Invalid Position Coordinates!\n";
            return IOB;
        }
    }
    else if(ch==2){
        totalRows=yRows;
        totalCols=yCols;
        /* Checking if pixel position is between bounds of frame */
        if(row < 0 || row >= getYBlockRows(*blk) || col < 0 || col >= getYBlockCols(*blk)){
            cout << "Invalid Position Coordinates!\n";
            return IOB;
        }
    }
    else if(ch==3){
        totalRows=zRows;
        totalCols=zCols;
        /* Checking if pixel position is between bounds of frame */
        if(row < 0 || row >= getZBlockRows(*blk) || col < 0 || col >= getZBlockCols(*blk)){
            cout << "Invalid Position Coordinates!\n";
            return IOB;
        }
    }
    else{
        cout << "Invalid channel number!\n";
        return ICE;
    }

    int prow = row*blk->getMaxRows();
    int pcol = col*blk->getMaxCols();
    
    /*subBlock arrangement*/
    if(totalRows-prow < blk->getMaxRows())
        blk->setRows(totalRows-prow);
    else
        blk->setRows(blk->getMaxRows());
    if(totalCols - pcol < blk->getMaxCols())
        blk->setCols(totalCols-pcol);
    else
        blk->setCols(blk->getMaxCols());

    blk->setXPos(pcol);
    blk->setYPos(prow);
        
    return 0;
}

int YUV420_Frame::setBlockVal(Block &blk, int row, int col, int ch){
    
    int totalRows, totalCols;
    
    if(ch==1){
		totalRows=xRows;
		totalCols=xCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getXRows() || col < 0 || col >= getXCols()){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==2){
		totalRows=yRows;
		totalCols=yCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getYRows() || col < 0 || col >= getYCols()){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==3){
		totalRows=zRows;
		totalCols=zCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getZRows() || col < 0 || col >= getZCols()){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else{
        cout << "Invalid channel number!\n";
        return ICE;
    }
    
    /*subBlock arrangement*/
    if(totalRows-row < blk.getMaxRows())
        blk.setRows(totalRows-row);
    else
        blk.setRows(blk.getMaxRows());
    if(totalCols - col < blk.getMaxCols())
        blk.setCols(totalCols-col);
    else
        blk.setCols(blk.getMaxCols());
    
    /* Go through the block and set the values */
    for(int i = 0; i < blk.getRows(); i++){
        for(int j = 0; j < blk.getCols(); j++){
            if(ch == 1)
                channel1[totalCols*(row+i) + col+j] = blk.getValue(i,j);  // Y
            else if(ch == 2)
                channel2[totalCols*(row+i) + col+j] = blk.getValue(i,j);  // U
            else
                channel3[totalCols*(row+i) + col+j] = blk.getValue(i,j);  // V
        }
	}
    return 0;
}

int YUV420_Frame::setExclusiveBlockVal(Block &blk, int row, int col, int ch){
    int totalRows, totalCols;
    
    if(ch==1){
		totalRows=xRows;
		totalCols=xCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getXBlockRows(blk) || col < 0 || col >= getXBlockCols(blk)){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==2){
		totalRows=yRows;
		totalCols=yCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getYBlockRows(blk) || col < 0 || col >= getYBlockCols(blk)){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else if(ch==3){
		totalRows=zRows;
		totalCols=zCols;
		/* Checking if pixel position is between bounds of frame */
		if(row < 0 || row >= getZBlockRows(blk) || col < 0 || col >= getZBlockCols(blk)){
			cout << "Invalid Position Coordinates!\n";
			return IOB;
		}
	}
	else{
        cout << "Invalid channel number!\n";
        return ICE;
    }
    
    int prow = row*blk.getMaxRows();
    int pcol = col*blk.getMaxCols();
    
    /*subBlock arrangement*/
    if(totalRows-prow < blk.getMaxRows())
        blk.setRows(totalRows-prow);
    else
        blk.setRows(blk.getMaxRows());
    if(totalCols - pcol < blk.getMaxCols())
        blk.setCols(totalCols-pcol);
    else
        blk.setCols(blk.getMaxCols());
    
    /* Go through the block and set the values */
    for(int i = 0; i < blk.getRows(); i++)
        for(int j = 0; j < blk.getCols(); j++){
            if(ch == 1)
                channel1[totalCols*(prow+i) + pcol+j] = blk.getValue(i,j);  // Y
            else if(ch == 2)
                channel2[totalCols*(prow+i) + pcol+j] = blk.getValue(i,j);  // U
            else
                channel3[totalCols*(prow+i) + pcol+j] = blk.getValue(i,j);  // V           
        }
    return 0;
}
