/* 
 * File:   Frame.hpp
 * Author: simon
 *
 * Created on October 7, 2014, 7:23 PM
 */

#ifndef FRAME_HPP
#define	FRAME_HPP

#include <iostream>
#include <stdio.h>

#include "Block.hpp"                      // Basic Block Operation and Structure 
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat)
#include <opencv2/highgui/highgui.hpp>  // Video write


#define OK  0     /* Success */
#define IOB 1     /* Index_Out_Of_Bounds - Position out of bounds of frame size */
#define UVF 2     /* Unknown_Video_Format  */
#define ICE 3     /* Invalid channel Error */  

unsigned const cn = 3;

using namespace std;
using namespace cv;

enum VideoFormat {
    RGB = 0,
    YUV_420 = 1
};

class Frame {
public:

    /** 
     * Destructor 
     */
    virtual ~Frame();

    /**
     * Decodes the raw data stream into 3 channel buffers each one with its component
     * 
     * @param src Pointer to the original buffer data
     */
    virtual void setData(unsigned char* src) = 0;
    
    /**
     * Converts a Frame into the specified video format
     * @param newFrame Address of new frame 
     * @param newFormat Format of new frame
     */
    int convert(Frame* newFrame, VideoFormat newFormat);

    /** Displays a frame 
     *   \param time Frame rate  
     */
    virtual void displayFrame(int time) =0;

        /** Return the 3 Component Matrix if already initialized[PODE NAO SER NECESSARIO]
     * 
     * @param c1 Pointer buffer with component R or Y
     * @param c2 Pointer buffer with component G or U
     * @param c3 Pointer buffer with component B or V
     */
    void getChannels(unsigned char** c1, unsigned char** c2, unsigned char** c3);
    
    /** Return the values of a given pixel 
     *   @param row Row position of the pixel
     *   @param col Column position the pixel
     *   @param val Pointer to the beggining of the array with the values RGB or YUV of the pixel 
     *   @return 
     *
     */
    virtual int getPixelVal(int row, int col, uchar val[]) =0;

    /** Sets the value of a channel of a pixel given its position of the indicated frame 
     *   @param row Row position of the pixel
     *   @param col Column position the pixel
     *   @param ch Channel where the value will be returned
     */
    virtual unsigned char getPixelValChannel(int row, int col, int ch) =0;
    
    /** Sets the value of a pixel given its position of the indicated frame 
     *   @param row Row position of the pixel
     *   @param col Column position the pixel
     *   @param val Pointer to the beggining of the array where the RGB or YUV values of the pixel 
     * will be saved
     *
     */
    virtual int setPixelVal(int row, int col, uchar val[])  =0;
    
    
    /** Sets the value of a channel of a pixel given its position of the indicated frame 
     *   @param row Row position of the pixel
     *   @param col Column position the pixel
     *   @param val Value of the component of the given pixel position
     *   @param ch Channel where the value will be set
     */
    virtual int setPixelValChannel(int row, int col, uchar val, int ch)  =0;

    /**
     * 
     * @param row X-axis position where the block starts
     * @param col Y-Axis position where the block starts
     * @param ch  Channel
     */
    virtual int getBlockVal(int row, int col, Block* blk, int ch) =0;
    
    /**
     * 
     * @param row X-axis position where the block starts
     * @param col Y-Axis position where the block starts
     * @param ch  Channel
     */
    virtual int getExclusiveBlockVal(int row, int col, Block* blk, int ch) =0;

    virtual int checkExclusiveBlockPos(int row, int col, Block* blk, int ch) =0;

    /**
     * 
     * @param blk Block to insert in the frame
     * @param row X-axis position where the block starts
     * @param col Y-axis position where the block starts
     * @param ch Channel
     */
    virtual int setBlockVal(Block &blk, int row, int col, int ch) =0;
    
    /**
     * 
     * @param blk Block to insert in the frame
     * @param row X-axis position where the block starts
     * @param col Y-axis position where the block starts
     * @param ch Channel
     */
    virtual int setExclusiveBlockVal(Block &blk, int row, int col, int ch) =0;

    /** 
     * If RGB data is in packed mode, if im YUV420 data is in planar mode
     * @return Pointer to the buffer which contains the data. 
     *              
     */
    virtual unsigned char* ptr() =0;
    
    /**
     * Returns a pointer to a packet mode buffer data   
     * @return Pointer to a packet mode buffer data 
     */
    virtual unsigned char* getDisplayBuffer() =0;
    
    /* Getters */
    int getFrameSize();
    VideoFormat getType();
    
    int getNRows();
    int getXRows();
    int getYRows();
    int getZRows();
    int getNCols();
    int getXCols();
    int getYCols();
    int getZCols();
    
    int getXBlockRows(Block &);
    int getYBlockRows(Block &);
    int getZBlockRows(Block &);
    int getXBlockCols(Block &);
    int getYBlockCols(Block &);
    int getZBlockCols(Block &);

    
  
protected:
    /**
     * Clamps a value to the range of 0 to 255.
     * 
     * @return value truncated
     * */
    unsigned char clamp(int val);

    /** Buffer to store data */
    unsigned char *buffer;
    /** Pointer to the value of component R or Y */
    unsigned char* channel1;
    /** Pointer to the value of component G or U */
    unsigned char* channel2;
    /** Pointer to the value of component B or V */
    unsigned char* channel3;
    
    /** Frame properties. */
    unsigned int nRows, nCols;
    unsigned int xRows, xCols, yRows, yCols, zRows, zCols;
    unsigned int frameSize;
    /** Format of video */
    VideoFormat type;
    

};

#endif	/* FRAME_HPP */

