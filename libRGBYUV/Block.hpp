/* 
 * File:   Block.hpp
 * Author: simon
 *
 * Created on October 7, 2014, 7:23 PM
 */

#ifndef BLOCK_HPP
#define	BLOCK_HPP

#include <iostream>
#include <stdio.h>


using namespace std;

class Block {
public:
    /** Default constructor for a 3x3 block */
    Block();
    /** Constructor of square blocks.
            \param rows number of rows
    \param cols number of cols*/
    Block(unsigned int rows, unsigned int cols);
    /** Destructor */
    ~Block();
    /** Print the content of the Block */
    void print();
    /** Set a value in a specific position of the Block.
            \param r position to set (row) 
    \param c position to set (col)
            \param val value to set */
    void setValue(unsigned int r, unsigned int c, unsigned char val);
    /** Return the value on the (r,c) position of the block */
    unsigned char getValue(unsigned int r, unsigned int c);
    /** Print the content of the Block using stream operators */
    friend ostream& operator<<(ostream& os, const Block& b);
    
    /** Block copy*/   
    void copyBlock(Block &block);
    
    /**Returns the number of rows that is using*/
    unsigned int getRows();
    /**Returns the number of cols that is using*/
    unsigned int getCols();
    /**Returns the max number of rows*/
    unsigned int getMaxRows();
    /**Returns the max number of cols*/    
    unsigned int getMaxCols();
    
    /**Sets the max number of rows*/
    void setRows(unsigned int rows);
    /**Sets the max number of cols*/    
    void setCols(unsigned int cols);

    double distance(const Block&);

    int getXPos();
    int getYPos();
    void setXPos(int);
    void setYPos(int);

private:
    /** Dimension of the block. */
    unsigned int nRows;
    unsigned int nCols;
    unsigned int maxRows;
    unsigned int maxCols;
    int xPos;
    int yPos;
    /** Buffer to store the block data (bSize x bSize). */
    unsigned char *buffer;
};

#endif	/* BLOCK_HPP */

