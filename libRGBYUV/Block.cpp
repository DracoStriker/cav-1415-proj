/* 
 * File:   Block.cpp
 * Author: simon
 * 
 * Created on October 7, 2014, 7:23 PM
 */

#include "Block.hpp"

#include <math.h>
#include <iostream>

using namespace std;

Block::Block() {
    maxRows=3;
    maxCols=3;
    nRows = maxRows;
    nCols = maxCols;
    buffer = new unsigned char[3 * 3];
}

Block::Block(unsigned int rows, unsigned int cols) {
    maxRows=rows;
    maxCols=cols;
    nRows = maxRows;
    nCols = maxCols;
    buffer = new unsigned char[rows * cols];
}

Block::~Block() {
    delete[] buffer;
}

void Block::print() {
    for (int r = 0; r < nRows; r++) {
        for (int c = 0; c < nCols; c++) {
            cout << (int) buffer[r * nCols + c] << "  ";
        }
        cout << endl;
    }
}

void Block::setValue(unsigned int r, unsigned int c, unsigned char val) {
    buffer[r * nCols + c] = val;
}

unsigned int Block::getRows() {
    return nRows;
}

unsigned int Block::getCols() {
    return nCols;
}

unsigned int Block::getMaxRows() {
    return maxRows;
}

unsigned int Block::getMaxCols() {
    return maxCols;
}

void Block::setRows(unsigned int rows){
    nRows=rows;
}

void Block::setCols(unsigned int cols){
    nCols=cols;
}

ostream& operator<<(ostream& os, const Block& b) {
    for (int r = 0; r < b.nRows; r++) {
        for (int c = 0; c < b.nCols; c++) {
            os << (int) b.buffer[r * b.nCols + c] << "  ";
        }
        os << endl;
    }

    return os;
}

void Block::copyBlock(Block &block)
{   
    for(int i=0; i<nRows; i++){
        for(int j=0; j<nCols; j++){
            block.buffer[i*nCols+j] = buffer[i*nCols+j];
            //cout<<"\nbuffer["<<i*maxCols+j<<"]: "<<(int) buffer[i*maxCols+j]<<endl;
        }
    }
}

unsigned char Block::getValue(unsigned int row, unsigned int col){
    return buffer[row*nCols+col];
}

double Block::distance(const Block& block) {
    double d = 0.0;
    //cout << "((" << nCols << ", " << nRows << "))" << endl;
    for (int r = 0; r < nRows; r++) {
        for (int c = 0; c < nCols; c++) {
            int idx = r*nCols+c;
            double sub = buffer[idx] - block.buffer[idx];
            d += sub*sub;
        }
    }
    return d;
}

int Block::getXPos() {
    return xPos;
}

int Block::getYPos() {
    return yPos;
}

void Block::setXPos(int xPos) {
    this->xPos = xPos;
}

void Block::setYPos(int yPos) {
    this->yPos = yPos;
}
