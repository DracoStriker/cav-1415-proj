/* 
 * File:   VideoFromCamera.cpp
 * Author: simon
 * 
 * Created on October 7, 2014, 7:55 PM
 */

#include "VideoFromCamera.hpp"

VideoFromCamera::VideoFromCamera(int device) : cap(device), device(device) {
    nCols = cap.get(CV_CAP_PROP_FRAME_WIDTH);
    nRows = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    fps = 30; //fps = cap.get(CV_CAP_PROP_FPS);
    mode = VideoFormat::RGB;
}

VideoFromCamera::VideoFromCamera(int device, unsigned int fps) : cap(device), device(device) {
    nCols = cap.get(CV_CAP_PROP_FRAME_WIDTH);
    nRows = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    this->fps = fps;
    mode = VideoFormat::RGB;
}

VideoFromCamera::~VideoFromCamera() {
}

int VideoFromCamera::readFrame(Frame &frame) {
    Mat buffer;
    if (!cap.read(buffer)) return -1;
    frame.setData(buffer.data);
    return 0;
}

ostream& operator<<(ostream &s, const VideoFromCamera &v) {
    return s << "Video(Height = " << v.nRows << ", Width = " << v.nCols << ", FPS = " << v.fps << ", Mode = " << v.mode << ", Device = " << v.device << ")";
}
