/* 
 * File:   Video.cpp
 * Author: simon
 * 
 * Created on October 7, 2014, 7:23 PM
 */

#include "Frame.hpp"
#include "RGB_Frame.hpp"
#include "YUV420_Frame.hpp"
#include "Video.hpp"

Video::~Video() {
}

unsigned int Video::getNCols() {
    return nCols;
}

unsigned int Video::getNRows() {
    return nRows;
}

unsigned int Video::getFps() {
    return fps;
}

VideoFormat Video::getMode() {
    return mode;
}

Frame *Video::genFrame() {
    switch (mode) {
        case RGB:
            return new RGB_Frame(nRows, nCols);
            break;
        case YUV_420:
            return new YUV420_Frame(nRows, nCols);
            break;
        default:
            return nullptr;
    }
}

Frame *Video::genConvToFrame() {
    switch (mode) {
        case RGB:
            return new YUV420_Frame(nRows, nCols);
            break;
        case YUV_420:
            return new RGB_Frame(nRows, nCols);
            break;
        default:
            return nullptr;
    }
}