/* 
 * File:   VideoFromCamera.hpp
 * Author: simon
 *
 * Created on October 7, 2014, 7:55 PM
 */

#ifndef VIDEOFROMCAMERA_HPP
#define	VIDEOFROMCAMERA_HPP

#include "opencv2/highgui/highgui.hpp"
#include "Frame.hpp"
#include "Video.hpp"

using namespace cv;
using namespace std;

class VideoFromCamera : public Video {
public:
    VideoFromCamera(int);
    VideoFromCamera(int, unsigned int);
    ~VideoFromCamera();
    int readFrame(Frame &);
    friend ostream& operator<<(ostream &, const VideoFromCamera &);
protected:
    VideoCapture cap;
private:
    int device;
};

#endif	/* VIDEOFROMCAMERA_HPP */

