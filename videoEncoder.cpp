/* 
 * File:   main.cpp
 * Author: simon
 *
 * Created on November 16, 2014, 1:16 PM
 */

#include <fstream>
#include <cstdlib>

#include <unistd.h>
#include <getopt.h>

#include "libCoding/JpegLs.hpp"
#include "libCoding/GolombRiceEncoder.hpp"
#include "libCoding/MotionCompensation.hpp"
#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/YUV420_Frame.hpp"
#include "libRGBYUV/VideoFromCamera.hpp"
#include "libRGBYUV/VideoFromFile.hpp"
#include "libPlot/cvplot.h"

using namespace std;

void printUsage(void);
int parseOpts(int, char**);
int encode(void);
bool fexists(const string&);
void swapFrames(Frame **, Frame **);

string fileName;
int dev = 0;
bool isDev = false;
bool plotOn = false;
unsigned short m = 2;
unsigned int Delta = 2;
unsigned int blkRows = 3;
unsigned int blkCols = 3;
unsigned int period = 0;
int bitCount = 0;
vector<int> samples;
vector<int> originalSamples;

int lossy = 0;
int factorY = 1, factorU = 1, factorV = 1;   

/*
 * 
 */
int main(int argc, char** argv) {
	
	samples.push_back(0);
	originalSamples.push_back(0);

	if (parseOpts(argc, argv)) return -1;


	return encode();
}

void printUsage(void) {
    cout << "videoEncoder [-f <filename>] [-c <camera>] [-m <golomb m>] [-l \"lossy\"] [-y <y factor>] [-u <u factor>] [-v <v factor>] [-I <period>] [-d <delta>] [ -b <block>]" << endl;
}

int parseOpts(int argc, char** argv) {
    int c;
    while ((c = getopt(argc, argv, "c:f:ly:u:v:hm:d:b:I:p")) != -1) {
        switch (c) {
            case 'c':
                isDev = true;
                dev = atoi(optarg);
                break;
            case 'f':
                isDev = false;
                fileName = optarg;
                break;
            case 'l':
                lossy = 1;
                break;
            case 'y':
                factorY = atoi(optarg);
                break;
            case 'u':
                factorU = atoi(optarg);
                break;
            case 'v':
                factorV = atoi(optarg);
                break;
            case 'm':
                m = atoi(optarg);
                break;
            case 'd':
                Delta = atoi(optarg);
                break;
            case 'b':
                blkRows = blkCols = atoi(optarg);
                break;
            case 'I':
                period = atoi(optarg);
                break;
            case 'p':
                plotOn = true;
                break;
            case 'h':
            default:
                printUsage();
                return -1;
        }
    }
    return 0;
}

int encode(void) {

	Video *v;

    if (isDev) {
        v = new VideoFromCamera(dev);
    } else {
        if (!fexists(fileName)) {
            cerr << "Error opening file " << fileName << " or file does not exist..." << endl;
            return -1;
        }
        v = new VideoFromFile(fileName);
    }

    Frame *f = v->genFrame();

    Frame *yuv = new YUV420_Frame(v->getNRows(), v->getNCols());

    Frame *previous; if (period != 0) previous = new YUV420_Frame(v->getNRows(), v->getNCols());

	string rawName = fileName.substr(0, fileName.find_last_of(".")); 

    ofstream ofs(rawName + ".gmb");

    ofs << v->getNCols() << " " << v->getNRows() << " " << v->getFps() << " " << m <<  " " << lossy << " " << factorY << " " << factorU << " " << factorV << " " << period << " " << Delta << " " << blkRows << " " << blkCols << endl;

    JpegLs jpg(lossy,factorY,factorU,factorV);

    MotionCompensation motion(Delta, blkRows, blkCols, factorY, factorU, factorV);

    GolombRiceEncoder golomb(m, &ofs);

    unsigned int deltasLen;

    unsigned int intraLen = yuv->getFrameSize();

    unsigned int interLen;

    if (period != 0) { 

        Block blk(blkRows, blkCols);

        deltasLen = interLen = intraLen + (yuv->getXBlockRows(blk) * yuv->getXBlockCols(blk) + yuv->getYBlockRows(blk) * yuv->getYBlockCols(blk) + yuv->getZBlockRows(blk) * yuv->getZBlockCols(blk)) * 2;

    } else deltasLen = intraLen;

    unsigned short *deltas = new unsigned short[deltasLen];

    int frame = 0;

    while (!v->readFrame(*f)) {

        cout << frame++ << endl;

        f->convert(yuv, YUV_420);

        if (period != 0) { 

            if ((frame - 1) % period != 0) { motion.encode(previous, yuv, deltas); deltasLen = interLen; }

            else { jpg.encode(yuv, deltas); deltasLen = intraLen; }

        } else jpg.encode(yuv, deltas);

        bitCount = 0;

    	for (int i = 0; i < deltasLen; i++) bitCount += golomb.encoder(deltas[i]);        //count the bits used in a frame

        samples.push_back(bitCount);

        originalSamples.push_back(intraLen*8);

        if (isDev) usleep(1.0 / v->getFps() * 1000000);

        if (period != 0) swapFrames(&yuv, &previous);
    }

    golomb.finalize();
    
    if (plotOn) {
		CvPlot::plot("Bitrate results - "+fileName+" | m = "+to_string(m)+" | modo "+(lossy ? "lossy" : "lossless") , (int*) samples.data(), (int) samples.size(), 1, 255, 0, 0);
		CvPlot::label("Bitrate comprimido");
		CvPlot::plot("Bitrate results - "+fileName+" | m = "+to_string(m)+" | modo "+(lossy ? "lossy" : "lossless") , (int*) originalSamples.data(), (int) samples.size(), 1, 0, 0, 255);
		CvPlot::label("Bitrate original");
		cvWaitKey(0);
	}

    return 0;
}

bool fexists(const string& filename) {
    ifstream ifile(filename.c_str());
    return ifile;
}

void swapFrames(Frame **f1, Frame **f2) {
    Frame *temp = *f1;
    *f1 = *f2;
    *f2 = temp;
}
