OBJS=$(patsubst %.cpp,%.o,$(wildcard libRGBYUV/*.cpp) $(wildcard libBit/*.cpp) $(wildcard libCoding/*.cpp) $(wildcard libPlot/*.cpp) $(wildcard libAudio/*.cpp) )

CC=g++
CFLAGS=-std=c++11 -I libRGBYUV -I libBit -I libCoding -I libPlot -lsndfile
PKG_CONFIG=`pkg-config opencv --cflags --libs`
PKG_CONFIG2=`pkg-config opencv --cflags`
BINARIES=videoComp videoConvert videoCopy videoEffects videoResize yuvShow videoEncoder videoDecoder videoPlayer audioEncoder audioDecoder

all: $(BINARIES)

test: test.cpp $(COBJS) $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(PKG_CONFIG)

audioEncoder: audioEncoder.cpp $(COBJS) $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(PKG_CONFIG)

audioDecoder: audioDecoder.cpp $(COBJS) $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(PKG_CONFIG)

videoComp: videoComp.cpp $(COBJS) $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(PKG_CONFIG)
	
videoConvert: videoConvert.cpp $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(PKG_CONFIG)

videoCopy: videoCopy.cpp $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(PKG_CONFIG)
	
videoEffects: videoEffects.cpp $(OBJS)
	$(CC) $^ -o $@ $(CFLAGS) $(PKG_CONFIG)
	
videoResize: videoResize.cpp $(OBJS)
	$(CC) $^ -o $@  $(CFLAGS) $(PKG_CONFIG)
	
yuvShow: yuvShow.cpp $(OBJS)
	$(CC) $^ -o $@  $(CFLAGS) $(PKG_CONFIG)

videoEncoder: videoEncoder.cpp $(OBJS)
	$(CC) $^ -o $@  $(CFLAGS) $(PKG_CONFIG)

videoDecoder: videoDecoder.cpp $(OBJS)
	$(CC) $^ -o $@  $(CFLAGS) $(PKG_CONFIG)

videoPlayer: videoPlayer.cpp $(OBJS)
	$(CC) $^ -o $@  $(CFLAGS) $(PKG_CONFIG)
	
%.o: %.c
	$(CC) -c $< -o $@ $(CFLAGS) 
	
%.o: %.cpp
	$(CC) -c $< -o $@ $(CFLAGS) $(PKG_CONFIG2)
	
cleanall: clean
	rm -f $(BINARIES) $(OBJS)
	
clean: 
	rm -f $(OBJS)
 