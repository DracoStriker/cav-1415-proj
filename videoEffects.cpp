/* 
 * File:   videoEffects.cpp
 * Author: simon
 *
 * Created on October 2, 2014, 2:07 AM
 */

#include <cstdlib>
#include "libRGBYUV/VideoFromFile.hpp"
#include "libRGBYUV/VideoFromCamera.hpp"
#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/VideoToFile.hpp"
#include "libRGBYUV/Video.hpp"

void bw();
void invertColors();
void luminanceFactor(double factor);
void subsampling(unsigned int totalFrames, unsigned int framesToShow);
void printUsage();
void parseArgs(int argc, char** argv);

using namespace std;

Video *video;

/*
 * 
 */
int main(int argc, char** argv) {
    if (argc < 3 || argc > 6) {
        printUsage();
    } else {
        if (string(argv[1])== "-c") {
            video = new VideoFromCamera(atoi(argv[2]));
            parseArgs(argc, argv);
        } else if (string(argv[1]) == "-f") {
            video = new VideoFromFile(argv[2]);
            parseArgs(argc, argv);
        } else{
            cout<<argv[1]<<endl;
            cout<<(argv[1]=="-f")<<endl;
            printUsage();
        }

    }
    return 0;
}

void bw() {
    Frame *inFrame = video->genFrame();
    Frame *outFrame = video->genFrame();
    string outFileName = inFrame->getType() == RGB ? "output.rgb" : "output.yuv";
    VideoToFile output(outFileName, *video);
    unsigned char bwValue;

    if (inFrame->getType() == RGB) {
        while (!video->readFrame(*inFrame)) {
            for (int i = 0; i < inFrame->getNRows(); i++) {
                for (int j = 0; j < inFrame->getNCols(); j++) {
                    bwValue = (inFrame->getPixelValChannel(i, j, 1) + inFrame->getPixelValChannel(i, j, 2) + inFrame->getPixelValChannel(i, j, 3)) / 3;
                    outFrame->setPixelValChannel(i, j, bwValue, 1);
                    outFrame->setPixelValChannel(i, j, bwValue, 2);
                    outFrame->setPixelValChannel(i, j, bwValue, 3);
                }
            }
            output.writeFrame(*outFrame);
        }
    } else {
        unsigned char trueValue;
        while (!video->readFrame(*inFrame)) {
            for (int i = 0; i < inFrame->getNRows(); i++) {
                for (int j = 0; j < inFrame->getNCols(); j++) {
                    trueValue = inFrame->getPixelValChannel(i, j, 1);
                    bwValue = (inFrame->getPixelValChannel(i, j, 2) + inFrame->getPixelValChannel(i, j, 3)) / 2;
                    outFrame->setPixelValChannel(i, j, trueValue, 1);
                    outFrame->setPixelValChannel(i, j, 127, 2);
                    outFrame->setPixelValChannel(i, j, 127, 3);
                }
            }
            output.writeFrame(*outFrame);
        }

    }
}

void invertColors() {
    Frame *inFrame = video->genFrame();
    Frame *outFrame = video->genFrame();
    string outFileName = inFrame->getType() == RGB ? "output.rgb" : "output.yuv";
    VideoToFile output(outFileName, *video);

    while (!video->readFrame(*inFrame)) {
        for (int i = 0; i < inFrame->getNRows(); i++) {
            for (int j = 0; j < inFrame->getNCols(); j++) {
                outFrame->setPixelValChannel(i, j, 255 - inFrame->getPixelValChannel(i, j, 1), 1);
                outFrame->setPixelValChannel(i, j, 255 - inFrame->getPixelValChannel(i, j, 2), 2);
                outFrame->setPixelValChannel(i, j, 255 - inFrame->getPixelValChannel(i, j, 3), 3);
            }
        }
        output.writeFrame(*outFrame);
    }
}

void luminanceFactor(double factor) {
    Frame *inFrame = video->genFrame();
    Frame *outFrame = video->genFrame();
    string outFileName = inFrame->getType() == RGB ? "output.rgb" : "output.yuv";
    VideoToFile output(outFileName, *video);

    if (inFrame->getType() == RGB) {
        unsigned char r, g, b;
        while (!video->readFrame(*inFrame)) {
            for (int i = 0; i < inFrame->getNRows(); i++) {
                for (int j = 0; j < inFrame->getNCols(); j++) {
                    //x*Y = x*0.2126 R + x*0.7152 G + x*0.0722 B
                    r = inFrame->getPixelValChannel(i, j, 1);
                    g = inFrame->getPixelValChannel(i, j, 2);
                    b = inFrame->getPixelValChannel(i, j, 3);

                    r *= factor;
                    g *= factor;
                    b *= factor;

                    outFrame->setPixelValChannel(i, j, r, 1);
                    outFrame->setPixelValChannel(i, j, g, 2);
                    outFrame->setPixelValChannel(i, j, b, 3);
                }
            }
            output.writeFrame(*outFrame);
        }
    } else {
        unsigned char y;
        while (!video->readFrame(*inFrame)) {
            for (int i = 0; i < inFrame->getNRows(); i++) {
                for (int j = 0; j < inFrame->getNCols(); j++) {
                    y = inFrame->getPixelValChannel(i, j, 1);
                    y *= factor;
                    outFrame->setPixelValChannel(i, j, y, 1);
                    outFrame->setPixelValChannel(i, j, inFrame->getPixelValChannel(i, j, 2), 2);
                    outFrame->setPixelValChannel(i, j, inFrame->getPixelValChannel(i, j, 3), 3);
                }
            }
            output.writeFrame(*outFrame);
        }

    }
}

void subsampling(unsigned int totalFrames, unsigned int framesToShow) {
    Frame *inFrame = video->genFrame();
    string outFileName = inFrame->getType() == RGB ? "output.rgb" : "output.yuv";
    VideoToFile output(outFileName, *video);
    unsigned int countTotal = 1;

    while (!video->readFrame(*inFrame)) {
        if (countTotal <= framesToShow) {
            output.writeFrame(*inFrame);
        }
        if (countTotal == totalFrames) {
            countTotal = 1;
        } else {
            countTotal++;
        }
    }
}

void printUsage() {
    cout << "./videoEffects <source flag> <source name> <options>" << endl << endl << "source flag:" << endl << "-c -> camera device" << endl << "-f -> video file" << endl << endl << "options:" << endl << "-b -> black and white" << endl << "-i -> invert colors" << endl << "-l -> change luminance factor <factor>" << endl << "-s -> subsampling ratio <total no. of frames> <no. of frames to show>" << endl;
}

void parseArgs(int argc, char** argv) {
    if (string(argv[3]) == "-b") {
        bw();
    } else if (string(argv[3]) == "-i") {
        invertColors();
    } else if (string(argv[3]) == "-l") {
        if (argc == 5) {
            luminanceFactor(atoi(argv[4]));
        }
    } else if (string(argv[3]) == "-s") {
        if (argc == 6) {
            subsampling(atoi(argv[4]), atoi(argv[5]));
        }
    }else {
            printUsage();
        }
}
