/* 
 * File:   main.cpp
 * Author: simon
 *
 * Created on November 16, 2014, 1:36 PM
 */

#include <fstream>
#include <cstdlib>
#include <opencv2/opencv.hpp>

#include <unistd.h>
#include <getopt.h>

#include "libCoding/JpegLs.hpp"
#include "libCoding/GolombRiceDecoder.hpp"
#include "libRGBYUV/Frame.hpp"
#include "libRGBYUV/RGB_Frame.hpp"
#include "libRGBYUV/YUV420_Frame.hpp"
#include "libCoding/MotionCompensation.hpp"

using namespace std;
using namespace cv;

void printUsage(void);
int parseOpts(int, char**);
int decode(void);
bool fexists(const string&);
void swapFrames(Frame **, Frame **);

string fileName;

/*
 * 
 */
int main(int argc, char** argv) {

    if (parseOpts(argc, argv)) return -1;

	return decode();
}

void printUsage(void) {
    cout << "videoPlayer -f <filename>" << endl;
}

int parseOpts(int argc, char** argv) {
    int c;
    while ((c = getopt(argc, argv, "c:f:hm")) != -1) {
        switch (c) {
            case 'f':
                fileName = optarg;
                break;
            case 'h':
            default:
                printUsage();
                return -1;
        }
    }
    return 0;
}

int decode(void) {

    if (!fexists(fileName)) {
        cerr << "Error opening file " << fileName << " or file does not exist..." << endl;
        return -1;
    }

    ifstream ifs(fileName);

    unsigned int nCols, nRows, fps;
    unsigned short m;
    unsigned int Delta, blkRows, blkCols, period;
    int lossy, factorY, factorU, factorV; 

	string rawName = fileName.substr(0, fileName.find_last_of(".")); 

	string line;
    getline(ifs, line);
	istringstream(line) >> nCols >> nRows >> fps >> m >> lossy >> factorY >> factorU >> factorV >> period >> Delta >> blkRows >> blkCols;

    Frame *f = new YUV420_Frame(nRows, nCols);

    Frame *previous; if (period != 0) previous = new YUV420_Frame(nRows, nCols);

    JpegLs jpg(lossy,factorY,factorU,factorV);

    MotionCompensation motion(Delta, blkRows, blkCols);

    GolombRiceDecoder golomb(m, &ifs);

    unsigned int deltasLen;

    unsigned int intraLen = f->getFrameSize();

    unsigned int interLen;

    if (period != 0) { 

        Block blk(blkRows, blkCols);

        deltasLen = interLen = intraLen + (f->getXBlockRows(blk) * f->getXBlockCols(blk) + f->getYBlockRows(blk) * f->getYBlockCols(blk) + f->getZBlockRows(blk) * f->getZBlockCols(blk)) * 2;

    }

    unsigned short *deltas = new unsigned short[deltasLen];

    deltasLen = intraLen;

    int frame = 0;

    cvNamedWindow(rawName.c_str(), CV_WINDOW_AUTOSIZE);

    Mat img(f->getNRows(), f->getNCols(), CV_8UC3);

    char inputKey = '?';

    bool playing = true;

    while(!ifs.eof()) {

        cout << frame++ << endl;

        if (period != 0) { 

            if ((frame - 1) % period != 0) deltasLen = interLen;

            else deltasLen = intraLen;

        }

    	for (int i = 0; i < deltasLen; i++) {
			deltas[i] = golomb.decoder();
		}

        //cout << "   " << deltasLen << endl;

		if (period != 0) { 

            if ((frame - 1) % period != 0) motion.decode(previous, deltas, f);

            else jpg.decode(deltas, f);

        } else jpg.decode(deltas, f);

        img.data = f->getDisplayBuffer();

        imshow(rawName.c_str(), img);

        if (playing)
            inputKey = waitKey(1.0 / fps * 1000);   /* wait according to the frame rate */
        else
            inputKey = waitKey(0); 

        switch ((char) inputKey) {
            case 'q':
                return 0;
                
            case 'p':
                playing = playing ? false : true;
                break;
        }

        if (period != 0) swapFrames(&f, &previous);
    }
}

bool fexists(const string& filename) {
    ifstream ifile(filename.c_str());
    return ifile;
}

void swapFrames(Frame **f1, Frame **f2) {
    Frame *temp = *f1;
    *f1 = *f2;
    *f2 = temp;
}
