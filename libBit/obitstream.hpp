/* 
 * File:   obitstream.hpp
 * Author: simon
 *
 * Created on November 8, 2014, 7:40 PM
 */

#ifndef OBITSTREAM_HPP
#define	OBITSTREAM_HPP

#include <iostream>
#include "bitwrapper.hpp"

using namespace std;

class obitstream {
public:
    obitstream(ostream *);
    virtual ~obitstream();
    void write(bitwrapper&, unsigned int);
    void finalize();
private:
    ostream *os;
    unsigned char *buf;
    bitwrapper bw;
    unsigned int len;
    unsigned int bits;
};

#endif	/* OBITSTREAM_HPP */

