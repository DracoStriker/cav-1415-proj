/* 
 * File:   ibitstream.cpp
 * Author: simon
 * 
 * Created on November 9, 2014, 2:38 PM
 */

#include "ibitstream.hpp"

ibitstream::ibitstream(istream *is) : is(is), len(100 * 8), buf(new unsigned char[len / 8]()), bw(buf, len / 8), tbits(len), bits(0) {
}

ibitstream::~ibitstream() {
    delete[] buf;
}

unsigned int ibitstream::read(bitwrapper& bw, unsigned int n) {
    unsigned int count = 0;
    for (int i = 0; i < n; i++) {
        if (bits == 0) {
            if (is->eof()) {
                return count;
            } else {
                is->read((char *)buf, len / 8);
                tbits = bits = is->gcount() * 8;
            }
        }
        bw.write(i, this->bw.read(tbits - bits));
        count++;
        bits--;
    }
    //for (int i = n; i < bw.size(); i++) {
    //    bw.write(i, 0);
    //}
    return count;
}
