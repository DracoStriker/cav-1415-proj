/* 
 * File:   ibitstream.hpp
 * Author: simon
 *
 * Created on November 9, 2014, 2:38 PM
 */

#ifndef IBITSTREAM_HPP
#define	IBITSTREAM_HPP

#include <iostream>
#include "bitwrapper.hpp"

using namespace std;

class ibitstream {
public:
    ibitstream(istream *);
    virtual ~ibitstream();
    unsigned int read(bitwrapper&, unsigned int);
private:
    istream *is;
    unsigned char *buf;
    bitwrapper bw;
    unsigned int len;
    unsigned int tbits;
    unsigned int bits;
};

#endif	/* IBITSTREAM_HPP */

