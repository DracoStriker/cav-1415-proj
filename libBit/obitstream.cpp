/* 
 * File:   obitstream.cpp
 * Author: simon
 * 
 * Created on November 8, 2014, 7:40 PM
 */

#include "obitstream.hpp"

obitstream::obitstream(ostream *os) : os(os), len(100 * 8), buf(new unsigned char[len / 8]()), bw(buf, len / 8), bits(0) {
}

obitstream::~obitstream() {
    delete[] buf;
}

void obitstream::write(bitwrapper& bw, unsigned int n) {
    for (int i = 0; i < n; i++) {
        this->bw.write(bits, bw.read(i));
        bits++;
        if (bits == len) {
            os->write((const char *)buf, len / 8);
            bits = 0;
        }
    }
}

void obitstream::finalize() {
    os->write((const char *)buf, bits / 8);
    if (bits % 8) {
        unsigned char final = buf[bits / 8] & ~(0xFF << (bits % 8));
        os->write((const char *)&final, 1);
    }
}
