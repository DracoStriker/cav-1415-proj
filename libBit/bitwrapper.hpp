/* 
 * File:   bitwrapper.hpp
 * Author: simon
 *
 * Created on November 8, 2014, 7:50 PM
 */

#ifndef BITWRAPPER_HPP
#define	BITWRAPPER_HPP

class bitwrapper {
public:
    bitwrapper(unsigned char *, unsigned int);
    virtual ~bitwrapper();
    unsigned char read (unsigned int);
    void write (unsigned int, unsigned char);
    unsigned int size();
private:
    unsigned char *buf;
    unsigned int len;
};

#endif	/* BITWRAPPER_HPP */

