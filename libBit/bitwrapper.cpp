/* 
 * File:   bitwrapper.cpp
 * Author: simon
 * 
 * Created on November 8, 2014, 7:50 PM
 */

#include "bitwrapper.hpp"

bitwrapper::bitwrapper(unsigned char *buf, unsigned int size) : buf(buf), len(size * 8) {
}

bitwrapper::~bitwrapper() {
}

unsigned char bitwrapper::read(unsigned int i) {
    return (unsigned char) (buf[i / 8] >> (i % 8)) & 0x01;
}

void bitwrapper::write(unsigned int i, unsigned char b) {
    if (b & 0x01)
        buf[i / 8] |= 1 << (i % 8);
    else 
        buf[i / 8] &= ~(1 << (i % 8));
}

unsigned int bitwrapper::size() {
    return len;
}